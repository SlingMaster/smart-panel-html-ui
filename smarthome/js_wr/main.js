﻿// ===============================================
// Main  |  main.js
// ===============================================

// Start =============================
window.onload = function() {
  window.setTimeout(function() {
    showUI();
  }, 100);
  addCrossdomainEventsListener();
};

// ===============================================
function clickControlESP(evt) {
  evt.stopPropagation();
  var cmd = evt.target.value;
  console.warn("clickControl | id:", cmd);
  switch (cmd) {
    case "restart":
    case "update":
    case "fan":
      sendCommandESP("web_srv", WEB_URL, cmd, 0);
      break;
    default:
      break;
  }
}

// ===============================================
function clickClearError(evt) {
  evt.stopPropagation();
  sendCommandESP("web_srv", WEB_URL, "clear", 0);
}

// ===============================================
function updateDisplayData(id, json) {
  var thw = parseFloat(json["thw"]).toFixed(1);
  updateLedDisplay("t", parseFloat(json["t"]).toFixed(1));
  updateLedDisplay("h", parseFloat(json["h"]).toFixed(1));
  updateLedDisplay("thw", thw);
  updateLedDisplay("statusLabel", getTimeFormatUTC(json["time"]));
  updateProgressBar(id, json);
  updateErrors(json);
  //setLedColor(thw);
  setProgressColor(thw);
  setPickupValuePosition(json);
  setButtonState(json);
}

// ===============================================
function updateErrors(json) {
  var error = isPresent(json["last_error"]) ? json["last_error"] : "";
  $(".cs_error").html(error);
}

// ===============================================
function createDisplays() {
  // console.info("updateDisplayData");
  var content =
    '<div class="ds_box">' +
    createLedDisplay("t", "temperature", "t", "") +
    createControlLedDisplay("h", "humidity", "h", "") +
    createLedDisplay("thw", "temperature hot water", "t", "") +
    createProgressLedDisplay("thw_progress") +
    
      '<div class="fan_box">' +
        '<div class="fan stop"></div>' +
      '</div>' +
   
  createTimeLedDisplay("connection esp web server");
  ("</div>");

  document.getElementById("displays").innerHTML = content;
  $("#srvState").toggleClass("ready", false);
  setProgressColor(0);
}

// ===============================================
function setButtonState(json) {
  if (json["source"] === "esp") {
    $("#btn_power_fan").toggleClass("select", json["fan_force"]);
  }
  $(".fan").toggleClass("stop", json["fan_stop"]);
}

// ===============================================
function setLedColor(val) {
  var delta = Math.ceil((val - 25) / 5);
  if (delta < 0) {
    delta = 0;
  }
  var imgPos = delta > 5 ? 100 : delta * 20;
  // console.info("setLedColor val:", imgPos);
  $("#temperatureState").css("backgroundPosition", imgPos + "% center");
}

// ===============================================
function setProgressColor(val) {
  var delta = Math.ceil((val - 25) / 5);
  if (delta < 0) {
    delta = 0;
  }
  var imgPos = delta > 5 ? 100 : delta * 20;
  if (delta > 4) {
    $(".state_progress:first-child").css("margin-right", "0%");
    $(".state_progress:first-child").css("width", imgPos - 10 + "%");
  } else {
    $(".state_progress:first-child").css("width", imgPos + "%");
  }
  $(".state_progress:last-child").css("width", 80 - imgPos + "%");

  // console.info("setLedColor val:", imgPos);
  // $("#temperatureState").css("backgroundPosition", (imgPos + "% center"));
}

// ===============================================
function setPickupValuePosition(json) {
  // DIAPAZON 5°C -------
  var DIAPAZON = 5;
  var MIN_POS = 25;
  // --------------------
  var pickup = parseInt(json["pickup"], 10);
  var pickupPos = (pickup - DIAPAZON - MIN_POS) * 2;
  $(".cs_diapazon_poz").css("left", pickupPos + "%");
  $(".cs_diapazon_info").html(pickup + "%");
}
