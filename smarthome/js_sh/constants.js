﻿// ===============================================
// Constants  |  constants.js
// ===============================================
// https://api.thingspeak.com/channels/135846/feeds.json
// JSC-ESP8266-SH | Bathroom |	Channel ID:	135846

// =============================
// const
// =============================
	var WEB_URL = "http://192.168.1.199";
	var CHANNEL = 135846;
	var MAX_GRAPH = 5;
	var CHIP = 772471;
	var CLOUD_FRAME = "web_graph";	
	var RESULTS = {"t1":60, "t2":720, "t3":43200};	
	var TIME_SCALE = {"t1":10, "t2":20, "t3":1440};	
	var RESULTS = {"t1":60, "t2":720, "t3":30};
	var TYPE = ["spline", "spline", "spline", "spline", "spline"];
	var RES_MAX = [30, null, 770, 40, null];	
	var RES_MIN = [-20, null, 700, 10, null];	
	var SYM = ["", "&deg;C", "%", " mmHg", "&deg;C", "%", ""];
	
// =============================
// vars
// =============================
	var intervalUpdateESP;
	var graph_count = 1;
	var lastGraph = "t1";
	
//===============================================
function getCloudData(jsonData) {
	var json = jsonData["feeds"][0]; 

	var data = new Object;
	data["air_t"] = json["field1"];
	data["air_h"] = json["field2"];
	data["air_p"] = json["field3"];
	data["home_t"] = json["field4"];
	data["home_h"] = json["field5"];
	data["time"] = json["created_at"];
	data["all_mem"] = 100;
	data["mem"] = 100;
	data["source"] = "cloud";
	return data;
}

//===============================================
function isFieldUpdate() {
		return true;
}