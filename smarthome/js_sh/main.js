﻿// ===============================================
// Main  |  main.js
// ===============================================

// Start =============================
window.onload = function () {
	window.setTimeout(function () {
		showUI();				
	}, 100);	
	addCrossdomainEventsListener();	
};

// ===============================================
function clickControlESP(evt) {
	evt.stopPropagation();
	var cmd = evt.target.value;
	console.warn("clickControl | id:", cmd);
	switch (cmd) {
		case "restart":
		case "update":
		case "relay1":
		case "relay2":
		case "relay3":
		case "relay4":
			sendCommandESP("web_srv", WEB_URL, cmd, 0);
			break;       
		default:
			break;
    }
}
// ===============================================
function updateDisplayData(id, json) {
	var air_t = parseFloat(json["air_t"]).toFixed(1);
	updateLedDisplay("air_t", air_t); 	
	updateLedDisplay("air_h", parseFloat(json["air_h"]).toFixed(1)); 
	updateLedDisplay("air_p", parseFloat(json["air_p"]).toFixed(1)); 
	updateLedDisplay("home_t", parseFloat(json["home_t"]).toFixed(1));
	updateLedDisplay("home_h", parseFloat(json["home_h"]).toFixed(1));	
	updateLedDisplay("statusLabel", getTimeFormatUTC(json["time"])); 
	//setLedColor(air_t);
	setProgressColor(air_t);
	updateProgressBar(id, json);
	setButtonState(json);
}

// ===============================================
function createDisplays() {
	// console.info("updateDisplayData");
	var content = 
		'<div class="ds_box">' +
			createLedDisplay("air_t", "temperature air", "t", "") +
			createProgressLedDisplay("thw_progress") +
			createLedDisplay("air_h", "humidity air", "h", "") +
			createLedDisplay("air_p", "pressure air", "p", "") +
			createLedDisplay("home_t", "temperature home", "t", "") +
			createLedDisplay("home_h", "humidity home", "h", "") +
			createTimeLedDisplay("connection esp web server") +
		'</div>';
		
	document.getElementById("displays").innerHTML =	content;
	$("#srvState").toggleClass("ready", false);
	setProgressColor(-30);
}

// ===============================================
function setButtonState(json) {
	if (json["source"] === "esp") {
		$("#r1").toggleClass("select", json["relay1"]);
		$("#r2").toggleClass("select", json["relay2"]);
		$("#r3").toggleClass("select", json["relay3"]);
		$("#r4").toggleClass("select", json["relay4"]);
	}
}

// ===============================================
function setLedColor(val) {
	var delta = Math.ceil((val + 20)/10);
	if (delta < 0) {
		delta = 0;		
	}
	var imgPos = ( delta > 5 ) ? 100 : delta * 20;
	console.info("setLedColor val:", imgPos);
	$("#temperatureState").css("backgroundPosition", (imgPos + "% center"));
}

// ===============================================
function setProgressColor(val) {
	var delta = Math.ceil((val + 20)/10);
	if (delta < 0) {
		delta = 0;		
	}
	var imgPos = ( delta > 5 ) ? 100 : delta * 20;
	if (delta > 4) {
		$('.state_progress:first-child').css("margin-right", "0%");
		$('.state_progress:first-child').css("width", ((imgPos -10) + "%"));
	} else {
		$('.state_progress:first-child').css("width", (imgPos + "%"));
	}
	$('.state_progress:last-child').css("width", ((80-imgPos) + "%"));
	
	// console.info("setLedColor val:", imgPos);
	// $("#temperatureState").css("backgroundPosition", (imgPos + "% center"));
}