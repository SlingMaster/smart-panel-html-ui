﻿// ===============================================
// Main  |  main.js
// ===============================================

// Start =============================
window.onload = function () {
	window.setTimeout(function () {
		showUI();				
	}, 100);	
	addCrossdomainEventsListener();	
};

// ===============================================
function showUI() {
	var obj_container = document.getElementById("win");
	if (obj_container != null) {
		obj_container.classList.add("css-fade");
	}
	var json =  {
		"mem": 0, 
		"fan_force": false, 
		"fan_stop": true, 
		"t" : 0, 
		"h":0, 
		"thw":0, 
		"time":"&#8226;", 
		"source": "start"
		};	

	updateDisplayData(CHIP, json);	
	ConnectionCloudThingspeak(graph_count, curProperties(lastGraph), closeSpiners);		
	window.setTimeout(function () {
		sendRequestForESP();
		window.setTimeout(function () { 
			// console.warn("start receive : ", $("#receiver").hasClass("receive"));			
			if ($("#receiver").hasClass("receive")) {
				$("#cloud_srv").toggleClass("select", true);
				sendRequestForCloud();
			}						
		}, 5000);
	}, 150);			
}

// ===============================================
function clickControlESP(evt) {
	evt.stopPropagation();
	var cmd = evt.target.value;
	console.warn("clickControl | id:", cmd);
	switch (cmd) {
		case "restart":
		case "update":
		case "fan":
			sendCommandESP("web_srv", WEB_URL, cmd, 0);
			break;       
		default:
			break;
    }
}

// ===============================================
function updateDisplayData(id, json) {
	// console.info("updateDisplayData");
	var statusTime = getTimeFormatUTC(json["time"]);
	var content = 
		createLedDisplay("temperature", "t", parseFloat(json["t"]).toFixed(1)) +
		createLedDisplay("humidity", "h", parseFloat(json["h"]).toFixed(1)) +
		createLedDisplay("temperature hot water", "t", parseFloat(json["thw"]).toFixed(1)) +
		createTimeLedDisplay(statusTime);

	document.getElementById("bathroom").innerHTML =	content;
	updateProgressBar(id, json);
	setButtonState(json);
}

// ===============================================
function setButtonState(json) {
	$("#btn_power_fan").toggleClass("select", json["fan_force"]);
	$(".fan").toggleClass("stop", json["fan_stop"]);
}
