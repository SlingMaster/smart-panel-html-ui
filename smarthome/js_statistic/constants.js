﻿// ===============================================
// Constants  |  constants.js
// ===============================================
// https://api.thingspeak.com/channels/117345/feeds.json
// JSC-ESP8266-SH | Bathroom |	Channel ID:117345

// =============================
// const
// =============================
	var WEB_URL = "http://192.168.1.77";
	var CHANNEL = 117345;
	var MAX_GRAPH = 4;
	var CHIP = 2334292; 
	var CLOUD_FRAME = "web_graph";
	
	var RESULTS = {"t1":60, "t2":720, "t3":43200};	
	var TIME_SCALE = {"t1":1, "t2":20, "t3":1440};	
	var RESULTS = {"t1":60, "t2":720, "t3":30};
	var TYPE = ["spline", "spline", "spline", "line"];
	var RES_MAX = [50, 40, null, 1];	
	var RES_MIN = [10, 10, null, 0];
	var SYM = ["", "&deg;C", "&deg;C", "%", ""];
	
// =============================
// vars
// =============================
	var intervalUpdateESP;
	var graph_count = 1;
	var lastGraph = "t1";
	
//===============================================
function getCloudData(jsonData) {
	var json = jsonData["feeds"][0]; 

	var data = new Object;
	data["thw"] = json["field1"];
	data["t"] = json["field2"];
	data["h"] = json["field3"];
	data["fan_stop"] = (json["field4"] === "0");
	data["time"] = json["created_at"];
	data["all_mem"] = 100;
	data["mem"] = 100;
	data["source"] = "cloud";
	return data;
}

//===============================================
function isFieldUpdate() {
	if (graph_count === 4) {
		document.getElementById("min_max").innerHTML = "";
		return false;
	}
	return true;
}