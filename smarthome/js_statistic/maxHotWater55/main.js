﻿// ===============================================
// Main  |  main.js
// ===============================================
// =============================
// const
// =============================
	var WEB_URL = "http://192.168.1.77";
	var CHANNEL = 117345;
// =============================
// vars
// =============================
	var TOUCH_DEVICE = true;
	var myScrollX;
	var DELTA_HOT_WATER = 5;
	var maxValue = 50; 
	// var maxValue = 50; 
	var lastGraph = "t1";
// Start =============================
window.onload = function () {
	document.addEventListener('touchmove', function (evt) {
	   evt.preventDefault();
	}, false);
	window.setTimeout(function () {
		showUI();	
	}, 100);	
	TOUCH_DEVICE = isTouchDevice();
};
 
function showUI() {	
	$("#statistic_cool_water").html(
		'<div id="chart_grid" class="cs_chart"></div>' + 
		'<div class="graph_container">' +
			'<div id="graph_wrapper" class="wrapper">' +
				'<div class="scroller_x_auto">' +
					'<div id="graph" class="cs_graph_content"></div>'+
				'</div>' +
			'</div>' +
		'</div>' +
		
		//'<div class="cs_duration">&#8226;</div>' +
		'<div id="average" class="cs_graph_title cs_button" tabindex="0" onclick="clickShowOnlyAverage(event);">' +
			'Temperature Hot Water' +
		'</div>' +
		'<div class="cs_graph_title humidity">Humidity and <div class="icon fan">-</div> Power Fan On</div>'
	);
	createGridDisplay(maxValue); 
	
	var obj_container = document.getElementById("win");
	if (obj_container != null) {
		obj_container.classList.add("css-fade");
	}
	myScrollX = new IScroll("#graph_wrapper", { /*probeType: 3,*/ scrollX: true, scrollY: false, tap: false});
	sendRequestForCloudMinMaxData(lastGraph, "day");			
}
// ===============================================
function createGridDisplay(maxValue) {
	var maxLine = 10;
	var content = '';
	for (var i = maxLine; i >= 0; i--) {
		var labelValue = (i>4) 
			? (i* Math.floor(maxValue/maxLine) + DELTA_HOT_WATER + "&deg;C") 
			: (i*25) + "%";
		var elColor = (i > 6) ? ' style="color:#C03;"' :'';
		var elStyle = (i === 4) ? ' style="opacity:0;"' : '';
		content += 
			'<div class="cs_item_grid"' + elStyle + '>' +
				'<div class="grid_label"' + elColor + '>' + labelValue + '</div>' + 
				'<div class="grid_label right"' + elColor + '>' + labelValue + '</div>' +
			'</div>';
	}
	$("#chart_grid").html(content);
}

//===============================================
function getAverageValueOfArray(array){
	var sum = 0;
	for(var i = 0; i < array.length; i++){
		sum += parseInt(array[i]);
	}
	return Math.floor(sum/array.length);
}

// ===============================================
function nextGraph(evt) {
	evt.stopPropagation();
	var id = evt.target.id.replace('i', "");
	interval = parseInt(id);
	if (isNaN(interval)) {return;}
	
	$(".cs_btn_ui").toggleClass("select", false);
	$(evt.target).toggleClass("select", true);
	$("#average").toggleClass("select", false);
	showOnlyAverage();
	// console.info("interval =", interval);
	window.setTimeout(function () {
		sendRequestForCloudMinMaxData(lastGraph, "day");
	}, 100);	

}

// ===============================================
function clickShowOnlyAverage(evt) {
	evt.stopPropagation();
	$(evt.target).toggleClass("select");
	showOnlyAverage();
}

// ===============================================
function showOnlyAverage() {
	var show = $("#average").hasClass("select") 
	var msg = show ? "Average Temperature Hot Water" : "Temperature Hot Water";
	$("#average").html(msg);
	$(".val_max").toggleClass("hide", show);
	$(".val_min").toggleClass("hide", show);
	$(".val_avg").toggleClass("only_avg", show);
	//console.info("interval =", interval);
}
/*
// ===============================================
function printJsonData(cmd, jsonData){
	var target = "json_list";
	if (isUndefined(document.getElementById(target))) {
		return;
	}
	document.getElementById(target).innerHTML = '<p style="color:#FFF">' + "chip ID  : " + cmd + '</p>';
	var json = jsonData;
	if (json !== null) {
		for (var itemKey in json) {
		document.getElementById(target).appendChild(
				document.createTextNode( ( itemKey + ' : ' + json[itemKey] + '\n'), null, 3));
		}
	}
}
*/
