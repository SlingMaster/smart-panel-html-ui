// ===============================================
// Cloud  |  cloud.js
// ===============================================
// =============================
// const
// =============================
	var MAX_ATTEMPS = 3;
	var HOT_WATER_MAX = 50;
	var FIELD_HOT_WATER = "field1";
	var FIELD_HUMIDITY = "field3";
	var FIELD_FAN = "field4";
// =============================
// vars
// =============================
	var interval = 30;
	var attempts_connection = MAX_ATTEMPS;
	var percent40 = 0;
	var percent45 = 0;
// ===============================================
function sendRequestForCloudMinMaxData(graph, results){	
	var RESULTS = {"t1":60, "t2":720, "t3":43200, "day":680};	
	$("#statistic_cool_water").append('<div id="spinner" class="graph_spinner"></div>');
	// ---------------------------------------
	//getServerSensorData("https://api.thingspeak.com/channels/" + 
	//	CHANNEL + "/fields/" + graph + ".json?results=" + RESULTS[results], formatStatisticData);
	getServerSensorData("https://api.thingspeak.com/channels/" + 
		CHANNEL + "/feeds.json?results=" + RESULTS[results], formatStatisticData);
		//CHANNEL + "/fields/" + graph + ".json", updateStatisticData);
	// ---------------------------------------
}

//===============================================
function formatStatisticData(jsonData) {
	var obj = jsonData["feeds"]; 
	//var field = "field" + graph_count;
	var arr = [];
	var arrHWT = [];
	var arrPeriod = [];
	var objStats = {}
	var lastKey = "";
	var t40 = 0;
	var t45 = 0;
	$("#spinner").fadeOut("slow", function() {
		removeObject("spinner");
	
		for (i = 0; i < obj.length; ++i) {
			if (!isNaN(obj[i][FIELD_HOT_WATER])) {
				var date = obj[i]["created_at"];
				var temperature = obj[i][FIELD_HOT_WATER];
				var curDate = moment(date).format("YYYY-MM-DD");
				var curTime = moment(date).format("HH");
				var key = curDate + " | " + curTime + ":" + getRoundMin(date);
				if (temperature >= 40 && temperature < 45) {
					t40++;
				}
				if (temperature >= 45) {
					t45++;
				}
				if (lastKey !== key) {
					lastKey = key;
					arr = new Array();
				}
				
				arrHWT.push(Math.round(temperature));
				arrPeriod.push(moment(date).format("DD MMMM"));
				arr.push( {
					"date": moment(date).format("DD.MMM"), 
					"time": curTime + ":" + getRoundMin(date),
					"firstHAH": isFirstHalfAnHour(date), 
					"hwt": Math.round(temperature),
					"humidity": Math.round(obj[i][FIELD_HUMIDITY]),
					"fan": Math.round(obj[i][FIELD_FAN]),
				});
				objStats[key] = arr;
			}
		}
		
	
		updateStatisticData(objStats, maxValue);
 
		var dataStr = uniqueKey(arrPeriod).toString().replace(',', " &#8226; ") + 
			" &#8226; " + moment(obj[obj.length-1]["created_at"]).format("YYYY");
		var word = moment(obj[obj.length-1]["created_at"]).format("MMMM");
		/*	
		var percentDurationHW = (getBrowser() === "MSIE") ?
			'<div class="ds stats">' + 
				'<div class="cs_label_stats">' + 'duration' +
					'<div class="icon t">-</div>> 40&deg;C &#8226; ' + 
						'<span>' + 
							parseFloat(t40/obj.length*100).toFixed(1) + 
						'%</span> ' +
					'<div class="icon t">-</div>> 45&deg;C &#8226; ' +
						'<span>' + 
							parseFloat(t45/obj.length*100).toFixed(1) + 
						'%</span>' +
				'</div>' +
			'</div>'
			: '';
		*/
		var percentDurationHW = 
			'<div class="ds stats">' + 
				'<div class="cs_label_stats">' + 'duration' +
					'<div class="icon t">-</div>> 40&deg;C &#8226; ' + 
						'<span>' + 
							parseFloat(t40/obj.length*100).toFixed(1) + 
						'%</span> ' +
					'<div class="icon t">-</div>> 45&deg;C &#8226; ' +
						'<span>' + 
							parseFloat(t45/obj.length*100).toFixed(1) + 
						'%</span>' +
				'</div>' +
			'</div>';

		
		$(".cs_graph_date").html(
			( arrPeriod[0].indexOf( word + 1) 
				? dataStr = dataStr.replace(word, "") : dataStr )
		);
		//$(".cs_duration").html( percentDurationHW );		
		$("#stats").html(
			'<div class="ds stats">' +
			'<div class="header_stats"></div>'+
			'</div>' +

			'<div class="ds stats">' + 
				'<div id="statusLabel" class="cs_label_stats">' + 
					'Temperature' +
					' Min &#8226; <span>' + Math.min.apply(null, arrHWT) + '&deg;C</span>' +
					' Avg &#8226; <span>' + getAverageValueOfArray(arrHWT) + '&deg;C</span>' +
					' Max &#8226; <span>' + Math.max.apply(null, arrHWT) + '&deg;C</span>' +
				'</div>' +
			'</div>' +
			'<div class="ds stats">' + 
				'<div id="statusLabel" class="cs_label_stats">' + 
					'<div class="icon t">-</div>Hot Water &#8226; <span>' + 
						parseFloat(obj[obj.length-1][FIELD_HOT_WATER]).toFixed(1) + 
						'&deg;C</span> ' +
					'<div class="icon h">-</div>Humidity &#8226; <span>' + 
						parseFloat(obj[obj.length-1][FIELD_HUMIDITY]).toFixed(1) + 
						'%</span>' +	
				'</div>' +
			'</div>' +
			percentDurationHW
		);
		
	});
}

//===============================================
function updateStatisticData(objStats, maxValue) {
	
	//console.info("objStats", objStats);
	var content = "";

	for (key in objStats) {
		var objDay = objStats[key];
		var arrT = new Array();
		var arrH = new Array();
		var arrF = new Array();

		for (i = 0; i < objDay.length; ++i) {
			arrT.push(objDay[i].hwt);
			arrH.push(objDay[i].humidity);
			arrF.push(objDay[i].fan);
		}
	
		content += 
			'<div class="box_val">' +
			
				createGraph(objDay[0]["date"], objDay[0]["time"], objDay[0]["firstHAH"], arrT, arrH, arrF) +
			'</div>';
	}
	$("#graph").html(content);
	//$(".cs_interval_val").html("Interval &#8226; " + interval + " min");
	$("#interval_val").html("set interval statistic value &#8226; " + interval + " min");
	
	$("#graph_wrapper").toggleClass( "x_mask", !$("#i30").hasClass("select") );
	
	myScrollX.refresh();
	window.setTimeout(function () {
		myScrollX.scrollTo(myScrollX.maxScrollX, 0, 2000, IScroll.utils.ease.circular);
	}, 100);
}

//===============================================
function isFirstHalfAnHour(date) {	
	return (Math.floor(parseInt( moment(date).format("mm")) / interval) === 0);
}

//===============================================
function getRoundMin(date) {
	return Math.floor(parseInt( moment(date).format("mm")) / interval);
}

//===============================================
function createGraph(date, time, firstHAH, arrT, arrH, arrF) {
	// maxValue = HOT_WATER_MAX;
	var maxHotWaterT = Math.max.apply(null, arrT); 
	var avgHotWaterT = getAverageValueOfArray(arrT);
	var minHotWaterT = Math.min.apply(null, arrT);
	
	var maxH = Math.max.apply(null, arrH); 
	var minH = Math.min.apply(null, arrH)-2; 
	var fanOn = (Math.max.apply(null, arrF) === 1); 

	//var maxValHW = Math.round(maxHotWaterT / maxValue * 100);
	//var avgValHW = Math.round(avgHotWaterT / maxValue * 100);
	//var minValHW = Math.round(minHotWaterT / maxValue * 100);
	var maxValHW = Math.round((maxHotWaterT - DELTA_HOT_WATER) / maxValue * 100);
	var avgValHW = Math.round((avgHotWaterT - DELTA_HOT_WATER) / maxValue * 100);
	var minValHW = Math.round((minHotWaterT - DELTA_HOT_WATER) / maxValue * 100);
	//var minValHW = Math.round((50 - DELTA_HOT_WATER) / maxValue * 100);
	var timeLabel = getTimeLabel(date, time, firstHAH);
	var deltaH = (maxH - minH) 
	var hot = (maxHotWaterT >= 40) ? ' hot_water' : '';
	var content = 
		'<div class="val_max' + hot + '" style="height:' + maxValHW + '%;"></div>' + 
		'<div class="val_avg" style="height:' + avgValHW + '%;"></div>' + 
		'<div class="val_min" style="height:' + minValHW + '%;"></div>' +
		'<div class="box_humidity">' +
			//'<div class="val_max_h" style="height:' + maxH + '%;"></div>' +
			//'<div class="val_min_h" style="height:' + minH + '%;"></div>' +
			'<div class="val_max_h" style="border-top-width:' + deltaH + 'px; height:' + maxH + '%; "></div>' +
			(fanOn ? '<div class="cs_fan_on"></div>': '') +
		'</div>' +
		timeLabel;
	/*
	$("#json_list").append(
		'<div class="cs_statistic">'+ 
			date + " | " + time + 
			//" | " + firstHAH + 
			" | " + minValHW +
			" | " + avgValHW + " | " + maxValHW + 
			" | " + maxH + "%" +
		'</div>'
	);
	*/
	return content;
}

//===============================================
function getTimeLabel(date, time, firstHAH) {
	var str = "";
	var modul = 3;
	var style_time = ' cs_style_min';
	var mins = time.substr(3, time.length);
	if (firstHAH){
		if (interval < 15) { 
			modul = 1;
		} 
		if (interval === 15) {
			modul = 2;
		}		
		
		var isEven = parseInt(time.substr(0,2)) % modul === 0;
		str = isEven ? (time.substr(0,3) === "00:" ? date : time.substr(0,3) + "00") : "";
		style_time = '';
		
	} else {
		if (interval < 15) {
			if (mins === "5") { str = time.substr(0,3) + "20";}
			if (mins === "10") { str = time.substr(0,3) + "40";}
		}
	}
	style_label = (time === "00:0") ? ' cs_style_date' : style_time;
	return (str === "") 
		? '' : '<div class="cs_time_label' + style_label + '"><span>&#8226;</span>' + str + '</div>';
}
