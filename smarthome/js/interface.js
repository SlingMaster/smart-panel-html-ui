// ===============================================
// Main  |  main.js
// ===============================================

// =============================
// const
// =============================
	var REQ_INIT = 1000;
// ===============================================
// variables
// ===============================================


// ===============================================
function sendRequestToAplication(requestID, cmd, jsonData) {
    //console.info("sendRequestToAplication | ", requestID, cmd, jsonData);

    if (withAPP) {
        window.setTimeout(function () {
            try {
                if (typeof jsonData === "string")
                    NativeInterface.callNative(requestID, cmd, jsonData);
                else
                    NativeInterface.callNative(requestID, cmd, JSON.stringify(jsonData));
            }
            catch (err) {
            }
        }, 10);
    } else {
        try {
            emulatorAplication(requestID, cmd, jsonData);
        }
        catch (err) {
        }
    }
}
// ===============================================
function testNativeApplication() {
    try {
        NativeInterface.callNative(REQ_INIT, "init", "{}");
		//console.info("testNativeApplication | Application Present");
        return true;
    }
    catch (err) {
        console.info("testNativeApplication | Application not Present");
        return false;
    }
}

// ===============================================
function isDefined(json) {
    return (json !== null && json !== undefined);
}