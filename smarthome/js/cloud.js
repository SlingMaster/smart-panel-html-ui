// ===============================================
// Cloud  |  cloud.js
// ===============================================
// =============================
// const
// =============================
	var MAX_ATTEMPS = 3;
// =============================
// vars
// =============================
	var attempts_connection = MAX_ATTEMPS;

//===============================================
function curProperties(id) {
	"use strict";
	var props = {
		"results":60,
		"timescale" : 1,	
		"type" : "spline",
		"yaxismax" : 60,
		"yaxismin" : 30
	};
	switch (id) {
		case "t1":
		case "t2":
		case "t3":
			lastGraph = id;
            break;
		default:
			break;
	}
	props.results = RESULTS[lastGraph];
	props.timescale = TIME_SCALE[lastGraph];	
	props.type = TYPE[graph_count-1];
	props.yaxismax = RES_MAX[graph_count-1];
	props.yaxismin = RES_MIN[graph_count-1];
	if (lastGraph === "t1" && graph_count < 4) {
		props.yaxismax = null;
		props.yaxismin = null;
	}
	return props;
}

// // =============================================== 
// function ConnectionCloudThingspeak(chart, props, callback) {
// 	document.getElementById("min_max").innerHTML = "connection";
// 	var URL = "https://thingspeak.com/channels/" + CHANNEL + "/charts/" + chart +
// 		"?bgcolor=%23000&color=%23C90&dynamic=true&results=" + props["results"] + 
// 		"&timescale=" + props["timescale"] + "&type=" + props["type"] +
// 		"&yaxismax="+ props["yaxismax"] + "&yaxismin=" + props["yaxismin"];
// 	var iframe = document.getElementById(CLOUD_FRAME);
// 	window.setTimeout(function () {
// 		iframe.src = URL;
// 		iframe.onload = function() {
// 			callback.call(); 
// 		};	
// 	}, 150);	
// }

// ===============================================
function sendRequestForCloud(){	
	"use strict";
	// console.info("sendRequestForCloud");			
	//$("#srvState").toggleClass("select", false);
	// ---------------------------------------
	getServerSensorData("https://api.thingspeak.com/channels/" + 
		CHANNEL + "/feeds.json?results=1", updateCloudData);	
	// ---------------------------------------
}

// ===============================================
function sendRequestForCloudMinMaxData(graph, results){	
	"use strict";
	var RESULTS = {"t1":60, "t2":720, "t3":43200};	

	if (isFieldUpdate()) {
		// ---------------------------------------
		getServerSensorData("https://api.thingspeak.com/channels/" + 
			CHANNEL + "/fields/" + graph + ".json?results=" + RESULTS[results], updateMinMaxData);
		// ---------------------------------------
	} else {
		$("#spiner").hide();
	}
}

//===============================================
function updateMinMaxData(jsonData) {
	"use strict";
	var obj = jsonData.feeds; 
	var field = "field" + graph_count;
	var arr = [];
	document.getElementById("min_max").innerHTML = "";

	for (i = 0; i < obj.length; ++i) {
		if (!isNaN(obj[i][field])) {
			 arr.push(obj[i][field]);
		}
	}
	document.getElementById("min_max").innerHTML =  
		"MIN &#8226; " + Math.min.apply(null, arr).toFixed(1) + 
		((SYM[graph_count] === " mmHg") ? "" : SYM[graph_count]) +
		" | MAX &#8226; " + Math.max.apply(null, arr).toFixed(1) + SYM[graph_count];
	closeSpiner();		
}

// ===============================================
function sendCommandESP(target, url, cmd, val) {
	"use strict";
	var sendData = url + "?cmd=" + cmd + "&val=" + val;
	var code = document.getElementById(target);
	// console.warn("sendCommandESP | target:", target, " | url:", sendData);
	code.src = sendData;
}

//===============================================
function updateCloudData(jsonData) {
	"use strict";
	// console.info("updateCloudData", jsonData);
	updateDisplay(CHIP, getCloudData(jsonData));
}

//===============================================
function updateDisplay(id, json) {
	"use strict";
	// printJsonData(id, json);
	updateDisplayData(id, json);
	$("#srvState").toggleClass("ready", true);
	attempts_connection = MAX_ATTEMPS;
	window.setTimeout(function () {
		$("#receiver").toggleClass("receive", false);	
	}, 500);
}

// ===============================================
function updateProgressBar(id, json) {
	"use strict";
	var content = '<div class="cs_label chip">cloud service<br />&#8226; thingspeak.com &#8226;</div>';
	if (json.source === "esp") {
		var all_mem = (json.all_mem === 0) ? 44444 : json.all_mem;
		var progress = 100 - 100/all_mem * json.mem;
		content = 
			//'<div class="cs_label">esp module info</div>' +
			'<div class="cs_label">esp module info<br />free memory &#8226; ' + json.mem + ' bytes</div>' +
			'<div class="progress">' +
				'<div class="bar" style="width:' + progress + '%"></div>' +
			'</div>' +
			'<div class="cs_label chip">chip id &#8226; ' + id + '</div>';
	}
	document.getElementById("chipESP").innerHTML =	content;
}

// ===============================================
function createLedDisplay(id, name, style, val) {
	"use strict";
	var content = 
		'<div class="ds ' + style + '">' +
			'<p>' + name + '</p>' +
			'<div id="' + id + '" class="val">' + val + '</div>' +
		'</div>';
	return content;
}

// ===============================================
function createControlLedDisplay(id, name, style, val) {
	"use strict";
	var content = 
		'<div class="ds big ' + style + '">' +
			'<p>' + name + '</p>' +
			'<div id="' + id + '" class="val">' + val + '</div>' +
		'</div>' +			
		createDiapazonLedDisplay(id, val);
	return content;
}

// ===============================================
function updateLedDisplay(id, val) {
	"use strict";
	var obj = document.getElementById(id);
	if (isDefined(obj)) {
		obj.innerHTML =	val;
	}
}

// ===============================================
function createProgressLedDisplay(id, val) {
	"use strict";
	var content = 
		'<div id="' + id + '" class="ds state">' +
			'<div class="state_progress"></div>' +
			'<div class="state_progress"></div>' +
		'</div>';
	return content;
}

// ===============================================
function createDiapazonLedDisplay(id, val) {
	"use strict";
	var content = 
			'<div class="ds diapazon">' +
			'<div class="cs_container fix" onClick="clickSetPickupValue(event);">' + 
					'<button id="dec" class="btn_diapazon s_dec"></button>' +
					'<div class="cs_diapazon_box">' + 
						'<span class="last fan_icon">&nbsp;</span>' +
						'<span class="cs_diapazon_info"></span>' +
						'<span>35</span>' +
						'<span>40</span>' +
						'<span>45</span>' +
						'<span>50</span>' +
						'<span>55</span>' +
						'<span>60</span>' +
						'<span>65</span>' +
						'<span class="last">70</span>' +
						'<div class="cs_diapazon_poz"></div>' +
					'</div>' +
					'<button id="inc" class="btn_diapazon s_inc"></button>' +		
			'</div>' +
		'</div>';

	return content;
}

// ===============================================
function createTimeLedDisplay(val) {
	"use strict";
	var content = 
		'<div id = "receiver" class="ds time receive">' +
			'<div id="statusLabel" class="cs_label time">' + val + '</div>' +
			'<div id="srvState" class="led_srv_state select"></div>' +
			// '<div id="temperatureState" class="led_color"></div>' +
			'<button class="cs_btn_restart" value="restart" ' + 
				'onClick="clickControlESP(event)">•</button>' +
		'</div>';
	return content;
}

// =============================================== 
function ConnectionThingspeak(frame_name, channel, chart, scale, results, type, callback) {
	"use strict";
	var URL = "https://thingspeak.com/channels/" + channel + "/charts/" + chart +
		"?bgcolor=%23000&color=%23C90&dynamic=true&results=" + results + 
		"&timescale=" + scale + "&type=" + type;
	var iframe = document.getElementById(frame_name);
	iframe.src = URL;
	iframe.onload = function() {
		callback.call(); 
  	};		
}

// ===============================================
function nextGraph(evt) {
	"use strict";
	evt.stopPropagation();
	var id = evt.target.id;
	switch (id) {
		 case "t1":
		 case "t2":
		 case "t3":
			$("#spiner").show();
		 	$(".btn_timeline").toggleClass("select", false);
			$(evt.target).toggleClass("select", true);									
            break;
		case "cloud_srv" :
			$(evt.target).toggleClass("select");
			$(".cs_btn_ui").toggleClass("disabled", $("#cloud_srv").hasClass("select"));
			$(".btn_diapazon").toggleClass("disabled", $("#cloud_srv").hasClass("select"));
			return;
        case "prev":
			clickVisualization(evt);	
			graph_count--;
			if (graph_count < 1) { graph_count = MAX_GRAPH; }
            break;
        case "next":
			clickVisualization(evt);
			graph_count++;
			if (graph_count > MAX_GRAPH) { graph_count = 1; }
            break;       
        default:
			return;
    }
	//console.info("nextGraph", frame_name, channel, graph_count, scale, type);	
	ConnectionCloudThingspeak(graph_count, curProperties(id), setMinMaxData);
}

// ===============================================
function clickVisualization(evt) {
	"use strict";
	evt.stopPropagation();
	$(evt.target).toggleClass("select", true);			
	$("#spiner").show();
	window.setTimeout(function () {
		$(evt.target).toggleClass("select", false);		
	}, 250);	
}

// ===============================================
function clickSetPickupValue(evt) {
	"use strict";
	evt.stopPropagation();
	var id = evt.target.id;
	switch (id) {
		 case "dec":
		 sendCommandESP("web_srv", WEB_URL, "dec", 0);
			break; 		 
			case "inc":
		 	sendCommandESP("web_srv", WEB_URL, "inc", 0);
		 	break;  
      
     default:
			return;
	}
}

// ===============================================
function setMinMaxData() {
	"use strict";
	sendRequestForCloudMinMaxData(graph_count, lastGraph);
}

// ===============================================
function closeSpiner() {
	"use strict";
	window.setTimeout(function () {
		$("#spiner").hide();
	}, 10);
}

// ===============================================
function startAutoSendRequest(){
	"use strict";
	intervalUpdateESP = setInterval(function () {
		$("#receiver").toggleClass("receive", true);
		if ($("#cloud_srv").hasClass("select")) {
			// alternative statick data
			sendRequestForCloud();
		} else {
			// dinamic esp data
			sendRequestForESP();
		}
		attempts_connection-- ;
		if (attempts_connection === 0) {
			$("#srvState").toggleClass("ready", false);
			$("#statusLabel").html("connection error");
			$("#cloud_srv").toggleClass("select", true);
			attempts_connection = MAX_ATTEMPS;
		}
		/*
		console.warn("sendRequest | receive : ", 
			$("#receiver").hasClass("receive"), " | attempts_connection", attempts_connection);
		*/	
	}, 5000);
}

// ===============================================
function sendRequestForESP(){
	"use strict";
	// console.info("sendRequestForESP");
	// ---------------------------------------
	sendCommandESP("web_srv", WEB_URL, "update", 0);
	// ---------------------------------------
}