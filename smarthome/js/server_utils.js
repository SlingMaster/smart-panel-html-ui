// ===============================================
// Variables
// ===============================================
//var URL_ENVIROMENT_DATA = "http://192.168.1.88/";
//var URL_ENVIROMENT_DATA = "http://192.168.1.172/sensors?dhttХ;dhthХ";
var URL_ENVIROMENT_DATA = "http://192.168.1.172/readjson";
// ===============================================

// ===============================================
function getEnviromentData(_url, callback_fn) {
	console.info("getEnviromentData | url:", _url);
    $.ajax({
        type: "GET",
        url: _url,
        dataType: 'json'})
            .done(function (jsonData) {
				console.info("done | jsonData:", jsonData);
				/*
                if (jsonData.status === "ZERO_RESULTS") {
                    console.error("Error of performance | status : ZERO_RESULTS");
                } else {
                    var callback = _callbacks[callback_fn];
                    callback(jsonData);
                }
				*/
            })
            .error(function () {
                console.error("Error of performance");
            });
}

// ===============================================
function sendServerData(jsonData) {
	console.info("sendServerData | jsonData:", jsonData);
	var format = "json";
	var _url = URL_ENVIROMENT_DATA + '?format=' + format;
	sendRequestToAplication(REQ_ENVIROMENT, "google_map",
			{"service_url": _url, "callback": z_save_callback(showJson)});

}
// ===============================================
function showJson(jsonData) {
	console.info("showJson | jsonData:", jsonData);
	var obj_container = document.getElementById("json_list");
	if (obj_container != null) {
		obj_container.innerHTML = 'jsonData';
	}
}

// ===============================================
// ===============================================
function initInterfase() {
    window.setTimeout(function () {
        $("#compas").toggleClass("hide", false);
        $(".gm-style-cc").css("opacity", 0);
    }, 2000);
}

// ===============================================
function hideMapInfo() {
    window.setTimeout(function () {
        $(".gm-style-cc").css("opacity", 0);
        //$(".gm-login-iframe").css("opacity" , 0); 
    }, 2500);
}