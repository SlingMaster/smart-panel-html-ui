// ===============================================
// Utils  |  utils.js
// ===============================================

// ===============================================
function getBrowser() {
	"use strict";
    var ua = navigator.userAgent;
    if (ua.search(/MSIE/) > 0)
        return "MSIE";
    if (ua.search(/Firefox/) > 0)
        return "Firefox";
    if (ua.search(/Opera/) > 0)
        return "Opera";
    if (ua.search(/Chrome/) > 0)
        return "Chrome";
    if (ua.search(/Safari/) > 0)
        return "Safari";
    if (ua.search(/Konqueror/) > 0)
        return "Konqueror";
    if (ua.search(/Iceweasel/) > 0)
        return "Iceweasel";
    if (ua.search(/SeaMonkey/) > 0)
        return "SeaMonkey";
    if (ua.search(/Gecko/) > 0)
        return "Gecko";
    return "N/A";
}

// ===============================================
function isTouchDevice() {
	"use strict";
    return (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent));
}

// ===============================================
function isDefined(json) {
	"use strict";
    return (json !== null && json !== undefined);
}

// ===============================================
function isUndefined(json) {
	"use strict";
    return (json === null || json === undefined);
}

// ===============================================
function isPresent(json) {
	"use strict";
    return (json !== null && json !== undefined && json !== "");
}

// ===============================================
function removeObject(id_name) {
	"use strict";
    var idElem = document.getElementById(id_name);
    if (idElem !== null) {
        idElem.parentNode.removeChild(idElem);
    }
}

// ===============================================
function noCallback(evt) {
	"use strict";
}

//===============================================
function uniqueKey(arr) {
	"use strict";
  var obj = {};

  for (var i = 0; i < arr.length; i++) {
    var str = arr[i];
    obj[str] = true;
  }

  return Object.keys(obj);
}

// ===============================================
function getTimeFormatUTC(val) {
	"use strict";
	var msUTC = Date.parse(val);
	var language = "en-US";
	// var language = "ru";
	var options = {
		// era: 'long',
		year: 'numeric',
		month: 'long',
		day: 'numeric',
		//weekday: 'long',
		timezone: 'UTC',
		hour: 'numeric',
		minute: 'numeric',
		second: 'numeric'
	};

	var valStr = isNaN(msUTC) ? "&#8226;" : new Date(msUTC).toLocaleString(language, options);
	return valStr;
}

// ===============================================
function getServerSensorData(_url, callback) {
	"use strict";
    $.ajax({
        type: "GET",
        url: _url,
        dataType: 'json'})
            .done(function (jsonData) {
                if (jsonData.status === "ZERO_RESULTS") {
                    console.error("Error of performance | status : ZERO_RESULTS");
                } else {
                     callback(jsonData);
                }
				
            })
            .error(function () {
                console.error("Error of performance");
				
            });
			
}