// ===============================================
// viewport.js
// ===============================================
function getViewpotContent(jsonData) {
    var wInch = isUndefined(jsonData["wInch"]) ? 8 : jsonData["wInch"];
    var DESIGN_SIZE = 1280;
    var content = "user-scalable=no, width=1280";

    var w = DESIGN_SIZE;
    var content = "user-scalable=no" + ", width=" + w;

    if (TABLET) {
        if (isiOS()) {
            content = "user-scalable=no, width=" + w;
        } else {
            if (getBrowser() === "Safari") {
                var dpi = Math.floor(DESIGN_SIZE / wInch);
                content = "user-scalable=no, target-densityDpi=" + dpi;
            } else {
                content = "user-scalable=no" + ", width=" + w;
            }
        }
    } else {
        w = 360;
        content = 'user-scalable=no, width=' + w;
    }

    /*
     $("#content_container").html('<P class="cs_debug">' +
     getViewportInfo() + '| <b>AU</b> | ' + content + ' | <br>' +
     ' | Window Inch : <b>' + wInch + '"</b>' + ' | Design Size : <b>' + DESIGN_SIZE + 'px</b>' +
     ' | Frame Size : <b>' + window.innerWidth + 'x' + window.innerHeight + '</b> | <br>' +
     ' | Browser : <b>' + getBrowser() + '</b>' +
     ' | Screen Size : <b>' + screen.availWidth + 'x' + screen.availHeight + '</b> | <br>' +
     ' | userAgent : <b>' + navigator.userAgent + '</b> |</P>');
     */
    return content;
}

// ===============================================
function getBrowser() {
    var ua = navigator.userAgent;
    if (ua.search(/MSIE/) > 0)
        return "MSIE";
    if (ua.search(/Firefox/) > 0)
        return "Firefox";
    if (ua.search(/Opera/) > 0)
        return "Opera";
    if (ua.search(/Chrome/) > 0)
        return "Chrome";
    if (ua.search(/Safari/) > 0)
        return "Safari";
    if (ua.search(/Konqueror/) > 0)
        return "Konqueror";
    if (ua.search(/Iceweasel/) > 0)
        return "Iceweasel";
    if (ua.search(/SeaMonkey/) > 0)
        return "SeaMonkey";
    if (ua.search(/Gecko/) > 0)
        return "Gecko";
    return "N/A";
}

// ===============================================
function isChrome() {
    return (getBrowser() === "Chrome");
}

// ===============================================
function changeMetaContentHTML(content) {
    if (content === "") {
        return false;
    }
    var viewport = document.querySelector("meta[name=viewport]");
    viewport.setAttribute('content', content);
}
/*
 // ===============================================
 function getViewportInfo() {
 var viewport = document.querySelector("meta[name=viewport]");
 var content = isDefined(viewport) ?
 viewport.getAttribute('content') : 'Viewport not Present';
 var content = '| <b>Start Vievport</b> |<br>' + content +
 '<br> | PixelRatio : <b>' + window.devicePixelRatio + '</b>' +
 ' | Screen Size : <b>' + screen.availWidth + 'x' + screen.availHeight + '</b><br>';
 return content;
 }
 */