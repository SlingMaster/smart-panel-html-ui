// ===============================================
// Main  |  main.js
// ===============================================
// https://api.thingspeak.com/channels/117345/feeds.json
// =============================
// const
// =============================
// var WEB_URL = "http://192.168.1.45";
// var CHIP = 1628406;

// =============================

var intervalUpdateESP;
var graph_count = 1;

// Start =============================
window.onload = function () {
	window.setTimeout(function () {
		showUI();				
	}, 100);
	
	addCrossdomainEventsListener();	
};

// ===============================================
function showUI() {
	var obj_container = document.getElementById("win");
	if (obj_container != null) {
		obj_container.classList.add("css-fade");
	}
	var json =  {
		"mem": 0, 
		"relay1": false,
		"relay2": false, 		
		"relay3": false,
		"relay4": false, 		
		"air_p" : 750.1,  
		"air_t" : "", 
		"air_h":"", 
		"home_t":"", 
		"home_h":"", 
		"time":"&#8226;", 
		"source": "start"
		};	

	updateDisplayData(CHIP, json);	
	ConnectionCloudThingspeak(graph_count, curProperties(lastGraph), closeSpiners);		
	window.setTimeout(function () {
		sendRequestForESP();
		window.setTimeout(function () { 
			// console.warn("start receive : ", $("#receiver").hasClass("receive"));
			
			if ($("#receiver").hasClass("receive")) {
				$("#cloud_srv").toggleClass("select", true);
				sendRequestForCloud();
			}						
		}, 5000);
	}, 150);			
}

// ===============================================
function clickControlESP(evt) {
	evt.stopPropagation();
	var cmd = evt.target.value;
	//$(evt.target).toggleClass("select");
	console.warn("clickControl | id:", cmd);
	switch (cmd) {
		case "update":
		case "relay1":
		case "relay2":
		case "relay3":
        case "relay4":
		 	sendCommandESP("web_srv", WEB_URL, cmd, 0);
            break;       
        default:
            break;
    }
}



// ===============================================
function updateDisplayData(id, json) {
	// console.info("updateDisplayData");
	var statusTime = getTimeFormatUTC(json["time"]);
	var pressure = isDefined(json["air_p"]) ? json["air_p"] : 0;
	//var pressStr = isNaN(pressure) ? "N/A" : pressure.toFixed(1);
	var content = 
		'<div class="ds_box">' +
			// createLedDisplay("pressure air", "p", (isDefined(json["air_p"]) ? json["air_p"].toFixed(1) : 0.0001)) +
			createLedDisplay("temperature air", "t", json["air_t"]) +
			createLedDisplay("humidity air", "h", json["air_h"]) +
			createLedDisplay("pressure air", "p", parseFloat(json["air_p"]).toFixed(1)) +
			createLedDisplay("temperature home", "t", json["home_t"]) +
			createLedDisplay("humidity home", "h", json["home_h"]) +
			createTimeLedDisplay(statusTime) +
		'</div>';
	document.getElementById("bathroom").innerHTML =	content;
	updateProgressBar(id, json);
	setButtonState(json);
}

// ===============================================
function setButtonState(json) {
	$("#r1").toggleClass("select", json["relay1"]);
	$("#r2").toggleClass("select", json["relay2"]);
	$("#r3").toggleClass("select", json["relay3"]);
	$("#r4").toggleClass("select", json["relay4"]);	
}
