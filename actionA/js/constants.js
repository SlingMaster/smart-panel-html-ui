// ===============================================
// constants
// ===============================================
var CMD_BACK = 3;
// events ======================
var EVT_NIGHT_MODE = 5100;
var EVT_NEXT = 5101;
var EVT_BACK_LIGHT = 5102;
// weather ---------------------
var EVT_WEATHER = 5000;
// =============================


var BULL = "&#8226;";
var BULL_SP = " &#8226; ";
var SPLASH_SHOW_DELAY = 2000;

var RESPONSE = "native_response",
	EXTERNAL_CTRL = "external_control",
	REQUEST = "client_event";

var CONTENT_PATCH = "img/"; 	
var MAP_ICON_PATCH = "img/map_icon/"; 
var EVT_NIGHT_MODE = 201;
