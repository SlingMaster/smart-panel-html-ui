/* global RESPONSE, EXTERNAL_CTRL, CMD_BACK, UNKNOWN */

/**
 * Created by Alex Dovbii on 18 May 2017.
 * Project AutoPilot
 * App Utils
 */

// ===============================================
function isPanScan() {
	return (window.innerWidth / window.innerHeight > 1.6);
}

// ===============================================
function isDefined(json) {
	return (json !== null && json !== undefined);
}

// ===============================================
function isUndefined(json) {
	return (json === null || json === undefined);
}

// ===============================================
function isPresent(json) {
	return (json !== null && json !== undefined && json !== "");
}

// ===============================================
function byBool(state) {
	return (state === true || state === "true");
}

// ===============================================
function notCarry() {
}

// ===============================================
function isObj(ID) {
	var element = document.getElementById(ID);
	var result = element ? true : false;
	return result;
}

// ===============================================
function removeObject(id_name) {
	var idElem = document.getElementById(id_name);
	if (idElem !== null) {
		idElem.parentNode.removeChild(idElem);
	}
}

// ===============================================
//create request to application in JSON format
function createJSONAppRequest(id, objJSON) {
	if (isUndefined(objJSON)) {
		return {"id": id, "content": {}};

	}
	return {"id": id, "content": objJSON};
}




// ===============================================
function animationItemScrollView(id) {
	var element;
	element = document.getElementById(id);
	if (element !== null) {
		window.setTimeout(function () {
			element.scrollIntoView();
		}, 500);
	}
}

// ===============================================
function isOdd(n) {
	var k = n / 2;
	return k === Math.floor(k);
}

// ===============================================
function sendMesssegeWindowParent(key, cmd, jsonData) {
	// view SendData from project to mDebuger
	var sendData = {"key": key, "cmd": cmd, "json": jsonData};
	window.parent.postMessage(JSON.stringify(sendData), '*');
	// ------------------------------------------------
}

// ===============================================
function addOnmessageEventsListener() {
	if (typeof window.addEventListener !== 'undefined') {
		window.addEventListener('message', onMessageCrossDomain, false);
	} else if (typeof window.attachEvent !== 'undefined') {
		window.attachEvent('onmessage', onMessageCrossDomain);

	}
}

// ===============================================
var onMessageCrossDomain = function (e) {
	e.stopPropagation();
	var dataString = e.data;
	var origin = e.origin;
	var cmd = "";
	var jsonData = {};
	if (isDefined(e.data)) {
		var data = ((typeof dataString) === "string") ? JSON.parse(dataString) : dataString;
		var key = data.key;
		
		console.log("[]");
		if (withAPP) {
			console.info("[res] onMessageCrossDomain | key: " + key + " | data: " + dataString);
		} else { 
			console.info("[res] onMessageCrossDomain | key: " + key + " | data: " + data);
		}
			// sendRequestToNativeApp(EVT_EXO_RESPONSE, {"key": key, "data": data});

		// response data from application -----------
		if (key === RESPONSE) {
			JavaScriptCallback(parseInt(data["cmd"]), data["json"]);
		}

		// ------------------------------------------
		// external control data from emulator ------
		if (key === EXTERNAL_CTRL ) {
			console.log(" ");
			//console.info("[ctrl] onMessageCrossDomain | key:", key, " | data:", data);
			externalControlUI(data.cmd, data["json"]);
		}
		// ------------------------------------------

		if (key === "client_req") {
			cmd = data.cmd;
			jsonData = data.jsonData;
			JavaScriptCallback(cmd, jsonData);
		}
	} else {
		console.error("onMessageCrossDomain | No Data Available ", origin);
	}
};

// ===============================================
function externalControlUI(cmd, jsonData) {
	console.info("externalControlUI", cmd);
	var state = jsonData["state"] || false;
	;
	switch (cmd) {
		case "id.refresh":
			location.reload(true);
			break;
		case "id.back":
			window.setTimeout(function () {
				JavaScriptCallback(CMD_BACK, {});
			}, 20);
			break;
		case "debug_view":
			$("div").toggleClass("no_stroke", state);
			$("li").toggleClass("no_stroke", state);
			$("ul").toggleClass("no_stroke", state);
			break;
		case "id.grid":
			$("div").toggleClass("cs_grid");
			break;
		case "overscan":
			$("div").toggleClass("hide_overflow", jsonData);
			console.info("overscan");
			break;
		case "id.scroll_down":
			$(".cs_scroll_y").animate({scrollTop: 8000}, "slow");
			break;
		case "id.scroll_up":
			$(".cs_scroll_y").animate({scrollTop: 0}, "slow");
			break;
	}
}

// ===============================================
// System Platform iOS and Android
// ===============================================
// ===============================================
function getBrowser() {
    var ua = navigator.userAgent;
    if (ua.search(/MSIE/) > 0)
        return "MSIE";
    if (ua.search(/Firefox/) > 0)
        return "Firefox";
    if (ua.search(/Opera/) > 0)
        return "Opera";
    if (ua.search(/Chrome/) > 0)
        return "Chrome";
    if (ua.search(/Safari/) > 0)
        return "Safari";
    if (ua.search(/Konqueror/) > 0)
        return "Konqueror";
    if (ua.search(/Iceweasel/) > 0)
        return "Iceweasel";
    if (ua.search(/SeaMonkey/) > 0)
        return "SeaMonkey";
    if (ua.search(/Gecko/) > 0)
        return "Gecko";
    return "N/A";
}

// ===============================================
function getAndroidVersion(ua) {
	ua = (ua || navigator.userAgent).toLowerCase();
	var match = ua.match(/android\s([0-9\.]*)/);
	return match ? match[1] : false;
}

// ===============================================
function isiOS() {
	return window.navigator.userAgent.match(/(iPhone|iPad|iPod)/);
}

// ===============================================
function iOSVersion() {
	return parseFloat(
			('' + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0, ''])[1])
			.replace('undefined', '3_2').replace('_', '.').replace('_', '')
			) || false;
}

// ===============================================
function isiPad() {
	return window.navigator.userAgent.match(/iPad/i);
}

// ===============================================
function isTouchDevice() {
	return (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent));
}

// ===============================================
function getValidDate(date) {
	if (isUndefined(date)) {
		return;
	}
	var validDate = new Date(date);
	return validDate;
}



// ===============================================
// load sync json files
// ===============================================
function createXHR() {
    var request = false;
    try {
        request = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (err2) {
        try {
            request = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (err3) {
            try {
                request = new XMLHttpRequest();
            }
            catch (err1) {
                request = false;
            }
        }
    }
    return request;
}

// ===============================================
function loadTextFile(fname, async) {
    var data = "";
    var xhr = createXHR();
    //var xhr=getXmlHttp();
    // async | true | false -----
    xhr.open("GET", fname, async);
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if (xhr.status !== 404) {
                data = xhr.responseText;
            }
        }
    };
    xhr.send(null);
    return data;
}

// ===============================================
// load JSON object from URL
function loadJSON(fname) {
    var data = loadTextFile(fname);
    if (data === null) {
        return {};
    }
    return JSON.parse(data);
}

/*
// ===============================================
function loadJsonData(urlFile, asyncFlag, callback) {
	$.ajax({
			url: urlFile,
			async: asyncFlag,
			success: function(data) {
			callback(data);
		},
		error: function() {
			console.warn("Error Loading");
		}
	});
}
*/

/*
// ===============================================
function getServerJsonData(_url, callback_fn) {
    $.ajax({
        type: "GET",
        url: _url,
        dataType: 'json'})
            .done(function (jsonData) {
                var callback = _callbacks[callback_fn];
                callback(jsonData);
            })
            .error(function () {
                console.error("Error of performance");
            });
}
*/