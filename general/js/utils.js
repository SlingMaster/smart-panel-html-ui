/* global RESPONSE, EXTERNAL_CTRL, CMD_BACK, UNKNOWN */

/**
 * Created by Alex Dovbii on 18 May 2017.
 * Project AutoPilot
 * App Utils
 */
function getTodVisual() {
	"use strict";
	var td = 0;
	var time = new Date();
	var HH = time.getHours();
	if ((HH < 3) || (HH > 20))  {td = 0;} // ночь
	if ((HH > 2)  && (HH < 9)) 	{td = 1;} // утро 
	if ((HH > 8) && (HH < 15))  	{td = 2;} // день
	if ((HH > 14) && (HH < 21)) 	{td = 3;} // вечер	
	return td;
}
function getTimeOfDay() {
	"use strict";
	var td = 0;
	var time = new Date();
	var HH = time.getHours();
	if ((HH < 3) || (HH > 20))  {td = 0;} // ночь
	if ((HH > 2)  && (HH < 9)) 	{td = 1;} // утро 
	if ((HH > 8) && (HH < 15))  	{td = 2;} // день
	if ((HH > 14) && (HH < 21)) 	{td = 3;} // вечер	
	return td;
}

// ===============================================
function isPanScan() {
	"use strict";
	return (window.innerWidth / window.innerHeight > 1.6);
}

// ===============================================
function isDefined(json) {
	"use strict";
	return (json !== null && json !== undefined);
}

// ===============================================
function isUndefined(json) {
	return (json === null || json === undefined);
}

// ===============================================
function isPresent(json) {
	"use strict";
	return (json !== null && json !== undefined && json !== "");
}

// ===============================================
function byBool(state) {
	"use strict";
	return (state === true || state === "true");
}

// ===============================================
function notCarry() {
	"use strict";
}

// ===============================================
function isObj(ID) {
	"use strict";
	var element = document.getElementById(ID);
	var result = element ? true : false;
	return result;
}

// ===============================================
function removeObject(id_name) {
	"use strict";
	var idElem = document.getElementById(id_name);
	if (idElem !== null) {
		idElem.parentNode.removeChild(idElem);
	}
}

// ===============================================
//create request to application in JSON format
function createJSONAppRequest(id, objJSON) {
	"use strict";
	if (isUndefined(objJSON)) {
		return {"id": id, "content": {}};

	}
	return {"id": id, "content": objJSON};
}




// ===============================================
function animationItemScrollView(id) {
	"use strict";
	var element;
	element = document.getElementById(id);
	if (element !== null) {
		window.setTimeout(function () {
			element.scrollIntoView();
		}, 500);
	}
}

// ===============================================
function isOdd(n) {
	"use strict";
	var k = n / 2;
	return k === Math.floor(k);
}

// ===============================================
function sendMesssegeWindowParent(key, cmd, jsonData) {
	"use strict";
	// view SendData from project to mDebuger
	var sendData = {"key": key, "cmd": cmd, "json": jsonData};
	window.parent.postMessage(JSON.stringify(sendData), '*');
	// ------------------------------------------------
}

// ===============================================
function addOnmessageEventsListener() {
	"use strict";
	if (typeof window.addEventListener !== 'undefined') {
		window.addEventListener('message', onMessageCrossDomain, false);
	} else if (typeof window.attachEvent !== 'undefined') {
		window.attachEvent('onmessage', onMessageCrossDomain);

	}
}

// ===============================================
var onMessageCrossDomain = function (e) {
	"use strict";
	e.stopPropagation();
	var dataString = e.data;
	var origin = e.origin;
	var cmd = "";
	var jsonData = {};
	if (isDefined(e.data)) {
		var data = ((typeof dataString) === "string") ? JSON.parse(dataString) : dataString;
		var key = data.key;
		
		// console.log("[]");
		// if (withAPP) {
		// 	console.info("[res] onMessageCrossDomain | key: " + key + " | data: " + dataString);
		// } else { 
		// 	console.info("[res] onMessageCrossDomain | key: " + key + " | data: " + data);
		// }
		console.info("[res] onMessageCrossDomain | key: " + key + " | data: " + dataString);
			// sendRequestToNativeApp(EVT_EXO_RESPONSE, {"key": key, "data": data});

		// response data from application -----------
		if (key === RESPONSE) {
			JavaScriptCallback(parseInt(data["cmd"]), data["json"]);
		}

		// ------------------------------------------
		// external control data from emulator ------
		if (key === EXTERNAL_CTRL ) {
			//console.info("[ctrl] onMessageCrossDomain | key:", key, " | data:", data);
			externalControlUI(data.cmd, data["json"]);
		}
		// ------------------------------------------

		if (key === "client_req") {
			cmd = data.cmd;
			jsonData = data.jsonData;
			JavaScriptCallback(cmd, jsonData);
		}
	} else {
		console.error("onMessageCrossDomain | No Data Available ", origin);
	}
};

// ===============================================
function externalControlUI(cmd, jsonData) {
	"use strict";
	console.info("externalControlUI", cmd);
	var state = jsonData["state"] || false;
	;
	switch (cmd) {
		case "id.refresh":
			location.reload(true);
			break;
		case "id.back":
			window.setTimeout(function () {
				JavaScriptCallback(CMD_BACK, {});
			}, 20);
			break;
		case "debug_view":
			$("div").toggleClass("no_stroke", state);
			$("li").toggleClass("no_stroke", state);
			$("ul").toggleClass("no_stroke", state);
			break;
		case "id.grid":
			$("div").toggleClass("cs_grid");
			break;
		case "overscan":
			$("div").toggleClass("hide_overflow", jsonData);
			console.info("overscan");
			break;
		case "id.scroll_down":
			$(".cs_scroll_y").animate({scrollTop: 8000}, "slow");
			break;
		case "id.scroll_up":
			$(".cs_scroll_y").animate({scrollTop: 0}, "slow");
			break;
	}
}

// ===============================================
// System Platform iOS and Android
// ===============================================
// ===============================================
function getBrowser() {
	"use strict";
    var ua = navigator.userAgent;
    if (ua.search(/MSIE/) > 0)
        return "MSIE";
    if (ua.search(/Firefox/) > 0)
        return "Firefox";
    if (ua.search(/Opera/) > 0)
        return "Opera";
    if (ua.search(/Chrome/) > 0)
        return "Chrome";
    if (ua.search(/Safari/) > 0)
        return "Safari";
    if (ua.search(/Konqueror/) > 0)
        return "Konqueror";
    if (ua.search(/Iceweasel/) > 0)
        return "Iceweasel";
    if (ua.search(/SeaMonkey/) > 0)
        return "SeaMonkey";
    if (ua.search(/Gecko/) > 0)
        return "Gecko";
    return "N/A";
}

// ===============================================
function getAndroidVersion(ua) {
	"use strict";
	ua = (ua || navigator.userAgent).toLowerCase();
	var match = ua.match(/android\s([0-9\.]*)/);
	return match ? match[1] : false;
}

// ===============================================
function isiOS() {
	"use strict";
	return window.navigator.userAgent.match(/(iPhone|iPad|iPod)/);
}

// ===============================================
function iOSVersion() {
	"use strict";
	return parseFloat(
			('' + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0, ''])[1])
			.replace('undefined', '3_2').replace('_', '.').replace('_', '')
			) || false;
}

// ===============================================
function isiPad() {
	"use strict";
	return window.navigator.userAgent.match(/iPad/i);
}

// ===============================================
function isTouchDevice() {
	return (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent));
}

// ===============================================
function getValidDate(date) {
	"use strict";
	if (isUndefined(date)) {
		return;
	}
	var validDate = new Date(date);
	return validDate;
}

// ===============================================
function formatXX(num, size) {
	"use strict";
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

// ===============================================
function formatSS(s, size, sym) {
    // var s = num+"";
    while (s.length < size) s = sym + s;
    return s;
}

//===============================================
function uniqueKey(arr) {
	"use strict";
  var obj = {};

  for (var i = 0; i < arr.length; i++) {
    var str = arr[i];
    obj[str] = true;
  }

  return Object.keys(obj);
}

// ===============================================
function getTimeFormatUTC(val) {
	"use strict";
	var msUTC = Date.parse(val);
	var language = "en-US";
	// var language = "ru";
	var options = {
		// era: 'long',
		year: 'numeric',
		month: 'long',
		day: 'numeric',
		//weekday: 'long',
		timezone: 'UTC',
		hour: 'numeric',
		minute: 'numeric',
		second: 'numeric'
	};

	var valStr = isNaN(msUTC) ? "&#8226;" : new Date(msUTC).toLocaleString(language, options);
	return valStr;
}

// ===============================================
function getServerSensorData(_url, callback) {
	"use strict";
    $.ajax({
        type: "GET",
        url: _url,
        dataType: 'json'})
            .done(function (jsonData) {
                if (jsonData.status === "ZERO_RESULTS") {
                    console.error("Error of performance | status : ZERO_RESULTS");
                } else {
                     callback(jsonData);
                }
				
            })
            .error(function () {
                console.error("Error of performance");
				
            });
			
}

// ===============================================
// load sync json files
// ===============================================
function createXHR() {
	"use strict";
    var request = false;
    try {
        request = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (err2) {
        try {
            request = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (err3) {
            try {
                request = new XMLHttpRequest();
            }
            catch (err1) {
                request = false;
            }
        }
    }
    return request;
}

// ===============================================
function loadTextFile(fname, async) {
	"use strict";
    var data = "";
    var xhr = createXHR();
    //var xhr=getXmlHttp();
    // async | true | false -----
    xhr.open("GET", fname, async);
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if (xhr.status !== 404) {
                data = xhr.responseText;
            }
        }
    };
    xhr.send(null);
    return data;
}

// ===============================================
// load JSON object from URL
function loadJSON(fname) {
	"use strict";
    var data = loadTextFile(fname);
    if (data === null) {
        return {};
    }
    return JSON.parse(data);
}

// ==============================================
// function loadXML() {
//   var xhr = new XMLHttpRequest();
//   xhr.open("GET", WEATHER_XML, true);

//   xhr.timeout = 2000; // time in milliseconds

//   xhr.onload = function() {
//     // Request finished. Do processing here.
//     var xmlDoc = this.responseXML; // <- Here your XML file
//     console.info("onload | XML:", xmlDoc);
//     console.info(
//       "XML:",
//       xmlDoc.getElementsByTagName("MMWEATHER")[0].childNodes[0].nodeValue
//     );
//   };

//   xhr.ontimeout = function(e) {
//     // XMLHttpRequest timed out. Do something here.
//   };

//   xhr.send(null);
// 

/*
// ===============================================
function loadJsonData(urlFile, asyncFlag, callback) {
	$.ajax({
			url: urlFile,
			async: asyncFlag,
			success: function(data) {
			callback(data);
		},
		error: function() {
			console.warn("Error Loading");
		}
	});
}
*/

/*
// ===============================================
function getServerJsonData(_url, callback_fn) {
    $.ajax({
        type: "GET",
        url: _url,
        dataType: 'json'})
            .done(function (jsonData) {
                var callback = _callbacks[callback_fn];
                callback(jsonData);
            })
            .error(function () {
                console.error("Error of performance");
            });
}
*/