// ===============================================
// Crossdomain | crossdomain.js
// ===============================================
function showUI() {
	"use strict";
  var obj_container = document.getElementById("win");
  if (obj_container != null) {
    obj_container.classList.add("css-fade");
  }

  createDisplays();
  sendRequestForESP();
  ConnectionCloudThingspeak(
    graph_count,
    curProperties(lastGraph),
    setMinMaxData
  );
  startAutoSendRequest();
}
// ===============================================
function sendCommandESP(target, url, cmd, val) {
	"use strict";
  var sendData = url + "?cmd=" + cmd + "&val=" + val;
  var code = document.getElementById(target);
  // console.warn("sendCommandESP | target:", target, " | url:", sendData);
  code.src = sendData;
}

// ===============================================
function addCrossdomainEventsListener() {
	"use strict";
  if (typeof window.addEventListener !== "undefined") {
    window.addEventListener("message", onMessageEvent, false);
  } else if (typeof window.attachEvent !== "undefined") {
    window.attachEvent("onmessage", onMessageEvent);
  }
}

// ===============================================
// onmessage view json data
// ===============================================
var onMessageEvent = function(e) {
	"use strict";
  e.stopPropagation();
  if ($("#cloud_srv").hasClass("select")) {
    return;
  }
  var dataStr = e.data;
  var origin = e.origin;
  //   console.warn("onMessageEvent " + dataStr, " | origin: " + origin);

  var data = JSON.parse(dataStr);
  var id = data.id;
  var json = data.json;
  // printJsonData(id, json);

  switch (id) {
    case CHIP:
      json["source"] = "esp";
      updateWeatherOutDoor(json);
      break;
    default:
      // console.warn("onMessageEvent", dataStr, " | origin:", origin);
      break;
  }
};

// ===============================================
// Callback Function
// ===============================================
var z_hashCode = function(s) {
	"use strict";
  var digit = s.split("").reduce(function(a, b) {
    a = (a << 5) - a + b.charCodeAt(0);
    return a & a; //intentional
  }, 0);

  return (digit >>> 0).toString();
};

// ===============================================
var z_save_callback = function(callback_fn) {
	"use strict";
  var callback_name = z_hashCode(callback_fn.toString());
  _callbacks[callback_name] = callback_fn;
  return callback_name;
};

// ===============================================
var z_get_callback = function(callback_name) {
	"use strict";
  return _callbacks[callback_name];
};

// ===============================================
function printJsonData(cmd, jsonData) {
	"use strict";
  var target = "json_list";
  if (isUndefined(document.getElementById(target))) {
    return;
  }
  document.getElementById(target).innerHTML =
    '<p style="color:#FFF">' + "chip ID  : " + cmd + "</p>";
  var json = jsonData;
  if (json !== null) {
    for (var itemKey in json) {
      document
        .getElementById(target)
        .appendChild(
          document.createTextNode(
            itemKey + " : " + json[itemKey] + "\n",
            null,
            3
          )
        );
    }
  }
}
