/**
 * Created by Alex Dovbii on 02 Jun 2017.
 * Project HTML5 UI|UX
 * Interface
 * Android Studio Application
 *
 * JavascriptInterface Android Constants -----------------------
 * public static final String INTERFACE_NAME = "NativeApplication";
 * function |   callNative   |
 * public final void callNative( int request, final String jsonString )
 * -------------------------------------------------------------
 * JavascriptInterface HTML Constants --------------------------
 * init events for all projects
 * var EVT_MAIN_TEST = 0;
 * var EVT_READY = 10;
 * function | callJavaScript |
 */
var RESPONSE = "native_response",
  EXTERNAL_CTRL = "external_control",
  REQUEST = "client_event";

var INTERFACE_NAME = "NativeApplication";
var REQUEST_JSON = "request",
  RESPONSE_JSON = "response";
// reserved for Html5UI --------
var CMD_DIRECTORY = 20;
var EVT_UI_PROJECT = 21;
var EVT_UI_RETURN = 22;
// -----------------------------

// init events and command -----
// for all projects ------------
var EVT_MAIN_TEST = 0;
var EVT_READY = 1;
var CMD_BACK = 4;
var EVT_SYNC = 5;
var CMD_INIT = 1000;

// commands ====================
// weather ---------------------
var CMD_WEATHER_DATA = 2000;
var CMD_SEASON_MAGIC = 2001;
var CMD_SWAP = 2100;
// =============================

// events ======================
var EVT_WOKE_UP = 5100;
var EVT_NEXT = 5101;
var EVT_BACK_LIGHT = 5102;
var EVT_SHOW_TIME = 5103;
// weather ---------------------
var EVT_WEATHER = 5000;
// =============================


// debug commands & events -----
var CMD_REDIRECT = 32;
var EVT_EXIT = 35;

var CMD_DEBUG = 13;
var EVT_EXO = 777;
var EVT_EXO_RESPONSE = 888;

var CHIP;

// ===============================================
function sendRequestToNativeApp(cmd, jsonData) {
	"use strict";
    // console.info("sendRequestToAplication | ", requestID, cmd, jsonData);

    if (withAPP) {
        window.setTimeout(function () {
            try {
                if (typeof jsonData === "string")
					window[INTERFACE_NAME].callNative(cmd, jsonData);
                else
					window[INTERFACE_NAME].callNative(cmd, JSON.stringify(jsonData));
            }
            catch (err) {
            }
        }, 10);
    }  else {
		// console.info("sendRequest cmd= " + cmd + " data=" + JSON.stringify(jsonData));
		sendMesssegeWindowParent(REQUEST, cmd, jsonData);
	}
}

// ===============================================
function isNativeApplication() {
	"use strict";
    try {
		window[INTERFACE_NAME].callNative(EVT_MAIN_TEST, "{}");
		console.info("[+] isNativeApplication | Application Present");
        return true;
    }
    catch (err) {
        console.info("[-] isNativeApplication | Application not Present");
        return false;
    }
}
