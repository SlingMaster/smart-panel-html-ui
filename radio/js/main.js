// ===============================================
// Main  |  main.js
// ===============================================

// =============================
// const
// =============================
var EVT_LIST = 1100;
var EVT_LIST = 1100;
var EVT_SAVE_PREFS = 1101;
var EVT_MUTE = 1102;

var CMD_FM_DATA = 1110;

var CMD_RADIO_PREV = 1115;
var CMD_RADIO_MUTE = 1116;
var CMD_RADIO_NEXT = 1117;
var CMD_RADIO_FAV1 = 1111;
var CMD_RADIO_FAV2 = 1112;
var CMD_RADIO_FAV3 = 1113;
var CMD_RADIO_FAV4 = 1114;
// EVT_UPDATE = 1001;
// EVT_SETTINGS = 1002;

// CMD_BACK = 2000;
// CMD_INIT = 3000;
// CMD_UPDATE = 4000;

// ===============================================
// variables
// ===============================================
var withAPP = false;
var radioList;
var mute = false;
var sound;
var lastIndex = -2;
var curIndex = 1;
var defMode = true;
var last_state = false;
var next_freq = 0;
var last_freq = 0;
var cycleID = 0;
var favorites = [];
//var list_url = "http://192.168.1.2/DEV_UI/SmartPanel/radio/fm_radio.json";
var list_url = "./radio/fm_radio.json";
// ===============================================

// Start =============================
window.onload = function () {
  "use strict";
  withAPP = isNativeApplication();
  curIndex -= 1;
  if (!withAPP) {
    radioList = loadJSON(list_url).fm;
    // window.localStorage.clear();
    resumeAppState();
    initUI();
    addOnmessageEventsListener();
  } else {
    sendRequestToNativeApp(EVT_READY, {
      browser: getBrowser(),
      ui: "project_radio",
    });
  }
  console.info("HTML onload");
};

// ===============================================
function initUI() {
  "use strict";
  createUI();
  setMute(true);
  window.setTimeout(function () {
    $(".overlay").fadeOut("slow", function () {});
    this.setInterval(setDigitalTime, 1000);
  }, 2500);
  //document.getElementById(body).add('<div class="cs_list_json">+++</div>');
  ResizeWidget();
  nextStation();
  // setStateControls();
  $("ul li").eq(curIndex).toggleClass("select", true);
}

// ===============================================
function createUI() {
  "use strict";

  $("#content").html(
    '<div class="box">' +
      '<div id="pos_content" class="pos_box">' +
      '<div id="fav_box" class="favorites">' +
      "</div>" +
      "</div>" +
      '<div id="analog" class="cs_content">' +
      '<div id="gear_big" class="gear_big"></div>' +
      '<div id="gear_litle" class="gear_litle"></div>' +
      '<div class="cs_widget">' +
      '<div id="cur_freq" class="cur_marker"></div>' +
      // '<div class="shadow"></div>' +
      '<div id="set_freq" class="regulator" onclick="next(event)">' +
      '<div id="prev" class="prev" onclick="onClickPrev(event)"></div>' +
      "</div>" +
      // '<div id="prev" class="prev_btn" onclick="onClickPrev(event)"></div>' +
      "</div>" +
      "</div>" +
      '<div id="radio_contect" class="display">' +
      '<div id="date" class="time_desc">•</div>' +
      '<div id="freq" class="fm"> </div>' +
      '<div id="station" class="time_desc litle"></div>' + 
      '<div id="btn_fav" class="fav_btn" onclick="onClickFavorites(event)"></div>' +
      
      '<div id="mute" class="mute_btn" onclick="onClickMute(event)">' +
      // '<div id="playing" class="playing">' +
      // '<div class="rect1"></div>' +
      // '<div class="rect2"></div>' +
      // '<div class="rect3"></div>' +
      // '<div class="rect4"></div>' +
      // '<div class="rect5"></div>' +
      
      // '<div class="bar"></div>' +
      // '<div class="bar"></div>' +
      // '<div class="bar"></div>' +
      // '<div class="bar"></div>' +
      // '<div class="bar"></div>' +      
      // '<div class="bar"></div>' + 

      // '<div class="loader-container">' +
      // '<div class="rectangle-1"></div>' +
      // '<div class="rectangle-2"></div>' +
      // '<div class="rectangle-3"></div>' +
      // '<div class="rectangle-4"></div>' +
      // '<div class="rectangle-5"></div>' +
      // '<div class="rectangle-6"></div>' +
      // '<div class="rectangle-5"></div>' +
      // '<div class="rectangle-5"></div>' +
      // '<div class="rectangle-4"></div>' +
      // '<div class="rectangle-3"></div>' +
      // '<div class="rectangle-2"></div>' +
      // '<div class="rectangle-1"></div>' + 
      // "</div>" +

      "</div>" +
      "</div>" +
      "</div>" +
      "</div>"
  );

  createListFM();
  setStateControls();
}
// ===============================================
function createListFM() {
  "use strict";
  var names = "";
  for (var i = 0; i < radioList.length; i++) {
    names += "<li>" + radioList[i].title + "</li>";
  }
  $("#fm_list").html('<ul id="fm_container">' + names + "</ul>");
  $("#fm_container").css("width", radioList.length * 120 + 120 + "px");
  createFavoritesList();
}
// ===============================================
function setStateControls() {
  "use strict";
  // var targetPos = defMode ? 735 : 825;
  var targetPos = defMode ? 675 : 765;
  var notFav = uniqueVal(favorites, curIndex);
  $("#btn_fav").css("opacity", notFav ? 0.5 : 1);
  $("#fm_container").css("left", targetPos - curIndex * 120 + "px");
  // $("#cur_freq").css("background-size", ((defMode ? "90% auto" : "100% auto")) );
  for (var i = 0; i < radioList.length; i++) {
    if (i < curIndex - 2 || i > curIndex + 1) {
      $("ul li").eq(i).css("opacity", 0);
    } else {
      $("ul li").eq(i).css("opacity", 1);
    }
    $("ul li")
      .eq(i)
      .toggleClass("select", i === curIndex);
  }
  $("ul li").eq(curIndex).toggleClass("select", true);
}

// ===============================================
window.onresize = function () {
  "use strict";
  ResizeWidget();
};

// ===============================================
// RADIO
// ===============================================
function next(evt) {
  "use strict";
  evt.stopPropagation();
  if (defMode) {
    defMode = false;
    swapDigitalAnalog();
  }
  curIndex += 1;
  nextStation();
}

// ===============================================
function onClickPrev(evt) {
  "use strict";
  evt.stopPropagation();
  if (defMode) {
    defMode = false;
    swapDigitalAnalog();
  }
  curIndex -= 1;
  nextStation();
}

// ===============================================
function nextStation() {
  // if (curIndex < 0) {
  //   curIndex = 0;
  //   return;
  // }
  if (curIndex >= radioList.length) {
    curIndex = 0;
    $("ul li").css("opacity", 0);
  }
  if (curIndex < 0) {
    curIndex = 0;
  }
  // console.info("curIndex • " + curIndex);
  var deg = (360 / radioList.length) * curIndex;
  next_freq = parseFloat(radioList[curIndex].freq);

  clearInterval(cycleID);
  cycleID = setInterval(setFreq, 10);
  $("#set_freq").css("-webkit-transform", "rotate(" + deg + "deg)");
  $("#prev").css("-webkit-transform", "rotate(" + -deg + "deg)");
  $("#gear_litle").css("-webkit-transform", "rotate(" + -deg * 0.5 + "deg)");
  $("#gear_big").css("-webkit-transform", "rotate(" + deg + "deg)");

  // $("#set_freq").css("-webkit-transform", "rotate(" + (1 + curIndex) * -30 + "deg)");

  // $("#date").html(
  //   "•  Station " + (curIndex + 1) + " of " + radioList.length + "  •"
  // );
  // $("#freq_pos").css("left", Math.floor(deg / 3.6) + "%");
  // $("#station").html(radioList[curIndex].title);
  if (defMode) {
    playRadio(curIndex);
  }
  // $("#cur_freq").css("background-size", ("90% auto") );
}

// ===============================================
function setFreq() {
  if (next_freq > last_freq) {
    last_freq += 0.1;
  } else {
    if (next_freq < last_freq) {
      last_freq -= 0.1;
    }
  }

  if (parseFloat(next_freq).toFixed(1) === parseFloat(last_freq).toFixed(1)) {
    clearInterval(cycleID);
    last_freq = next_freq;
    setStateControls();
  }
  $("#freq").html(formatSS(parseFloat(last_freq).toFixed(1), 5, " "));
}

// ===============================================
function setMute(flag) {
  if (isUndefined(flag)) {
    mute = !mute;
  } else {
    mute = flag;
  }
  console.info("set mute • " + mute + " | flag " + flag);
  $("#mute").toggleClass("select", mute);
  // $("#playing").toggleClass("select", mute);
  if (sound) {
    // sound.mute(true);
    if (mute) {
      sound.fade(1, 0, 2000);
      // $("#playing").toggleClass("select", mute);
    } else {
      mute = false;
      sound.fade(0, 1, 2000);
    }
    sendRequestToNativeApp(EVT_MUTE, {
      state: mute,
    });
  }
}
// ===============================================
function playRadio(index) {
  var data = radioList[index];
  if (lastIndex !== index) {
    if (sound) {
      //sound.fade(1, 0, 0);
      //mute = true;
      setMute(true);
      sound.unload();
    }
    // If we already loaded this track, use the current one.
    // Otherwise, setup and load a new Howl.
    if (data.howl) {
      sound = data.howl;
    } else {
      sound = data.howl = new Howl({
        src: data.src,
        html5: true, // A live stream can only be played through HTML5 Audio.
        format: ["mp3", "aac", "mpeg"],
      });
    }
    last_freq = parseFloat(data.freq);
    setMute(false);
    sound.play();
    lastIndex = index;
  }

  var deg = (360 / radioList.length) * curIndex;
  $("#cur_freq").css("-webkit-transform", "rotate(" + deg + "deg)");
  setStateControls();
  saveAppState();
  //mute = false;
  //sound.fade(0, 1, 1000);
}

// ===============================================
function ResizeWidget() {
  "use strict";
  var scale = Math.min(window.innerWidth, window.innerHeight) / 800;
  // var obj_win = document.getElementById("win");

  // if (obj_win != null) {
  //   scale = Math.min(obj_win.clientWidth, obj_win.clientWidth) / 1280;
  // }
  scale = Math.min($("#win").innerWidth(), $("#win").innerHeight()) / 720;
  var a_scale = defMode ? 0.65 : 1;
  var d_scale = defMode ? 1 : 0.75;
  var a_pos = defMode ? -108 : -95;
  var d_pos = defMode ? -22 : -10;
  // $("#date").css("text-align", defMode ? "center" : "right");
  $("#analog").css(
    "transform",
    "translate(" + Math.floor(a_pos) + "%, -50%) scale(" + scale * a_scale + ")"
  );
  $("#radio_contect").css(
    "transform",
    "translate(" + Math.floor(d_pos) + "%, -50%) scale(" + scale * d_scale + ")"
  );

  // $("#date").css("text-align", defMode ? "center" : "right");$
  $("#date").css("color", defMode ? "#84ffff" : "#FFFFFF");
  // $("#date").css("color", defMode ? "#84ffff" : "#FFFFFF");
  // $("#date").html(" ");
  $("#freq").css("color", defMode ? "#FFFFFF" : "#84ffff");

  // $("#station").css(
  //   "border-top-color",
  //   defMode ? "rgba(255,255,255, 0)" : "rgba(255,255,255, .35)"
  // );
  $("#pos_content").css("opacity", defMode ? 0 : 1);
  $("#station").css("font-size", defMode ? "36px" : "24px");
  $(".bg").css("background-position", defMode ? "center 72%" : "center 45%");

  window.setTimeout(function () {
    $("#cur_freq").css("background-size", defMode ? "90% auto" : "100% auto");
  }, defMode ? 2500 : 1);
  last_state = defMode;
}

// ===============================================
function setDigitalTime() {
  "use strict";
  if (defMode) {
    var MONTH = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    var WEEKDAY = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];
    var date = new Date();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();
    $("#date").html(
      '<div class="time_desc">' +
        date.getDate() +
        ", " +
        MONTH[date.getMonth()] +
        " " +
        date.getFullYear() +
        " • " +
        WEEKDAY[date.getDay()] +
        " • " +
        formatXX(hour, 2) +
        "<span>:</span>" +
        formatXX(min, 2) +
        "<span>:</span>" +
        formatXX(sec, 2) +
        "</div>"
    );
  }
}

// ===============================================
function onClickSettings(evt) {
  "use strict";
  evt.stopPropagation();
  //location.reload(true);
  sendRequestToAplication(EVT_SETTINGS, "settings", {});
}
// ===============================================
function onClickSwap(evt) {
  "use strict";
  evt.stopPropagation();
  defMode = !defMode;
  swapDigitalAnalog();
  if (defMode) {
    playRadio(curIndex);
  }
}

// ===============================================
function swapDigitalAnalog() {
  "use strict";
  console.info("swapDigitalAnalog | defMode " + defMode);
  ResizeWidget();
}

// ===============================================
function init(jsonData) {
  "use strict";
  var prefs;
  if (isDefined(jsonData.prefs)) {
    prefs = JSON.parse(jsonData.prefs);
    curIndex = prefs.cur_index || 0;
    if (isDefined(prefs.favorites)) {
      var arr = prefs.favorites.split(",");
      // if (isDefined(arr)) {
      for (var i = 0; i < arr.length; i++) {
        // console.info("INIT | arr[" + i + "]<" + arr[i] + ">");
        favorites.push(arr[i]);
      }
      // }
    }
    console.info(
      "INIT | jsonData : " +
        jsonData.prefs +
        " Favorites : " +
        JSON.stringify(favorites)
    );
  }
  sendRequestToNativeApp(EVT_LIST, {});
}

// ===============================================
function JavaScriptCallback(cmd, jsonData) {
  "use strict";
  // cmd = parceInt(cmd, 10);
  // console.info(
  //   "[] JavaScriptCallback cmd = " +
  //     cmd +
  //     " | data = " +
  //     JSON.stringify(jsonData)
  // );

  var response = jsonData[RESPONSE_JSON] || {};
  // outputJson(syntaxHighlight(response));
  // if (cmd !== EVT_EXO) {
  //   $("#json-cmd").html(
  //     "cmd : " + cmd + " | " + JSON.stringify(response).split(",").join(", ")
  //   );
  // }

  switch (cmd) {
    case CMD_INIT:
      init(response);
      break;
    case CMD_BACK:
      backEvent();
      break;
    case CMD_WEATHER_DATA:
      // forecastWeather(response);
      break;
    case CMD_FM_DATA:
      radioList = response.fm;
      initUI();
      nextStation();
      break;
    case CMD_RADIO_PREV:
      curIndex -= 1;
      nextStation();
      // $("#json-cmd").html(
      //   "cmd : " + cmd + " | " + JSON.stringify(response).split(",").join(", ")
      // );
      break;
    case CMD_RADIO_MUTE:
      defMode = !defMode;
      ResizeWidget();
      setMute(!defMode);
      break;
    case CMD_RADIO_NEXT:
      curIndex += 1;
      nextStation();
      break;
    case CMD_RADIO_FAV1:
      nextFavorites(0);
      break;
    case CMD_RADIO_FAV2:
      nextFavorites(1);
      break;
    case CMD_RADIO_FAV3:
      nextFavorites(2);
      break;
    case CMD_RADIO_FAV4:
      nextFavorites(3);
      break;
    // --------------------------
    // Watch
    // --------------------------
    case CMD_SWAP:
      defMode = !defMode;
      // $("#json-cmd").html("cmd : " + cmd + " | defMode • " + defMode);
      ResizeWidget();
      break;
    // --------------------------
    case CMD_REDIRECT:
      // backEvent();
      break;
    case EVT_EXO:
      //  outputJson(syntaxHighlight(response));
      // $("#json-cmd").append(
      //   "<br />cmd : " +
      //     cmd +
      //     " | " +
      //     JSON.stringify(response).split(",").join(", ")
      // );
      break;
    default:
      console.info("[ ] JavaScriptCallback command " + cmd + " not present");
      break;
  }
}

// ===============================================

// ===============================================
/*function backPrevScreen() {
  "use strict";
  console.info("backPrevScreen");
}


function updateWeatherOutDoor(json) {}

// ===============================================
function printJsonData(target, cmd, jsonData){
	if (withAPP) {return;}	
	document.getElementById("json").innerHTML = '<p style="color:#FC0">' + " json  : " + target + '</p>';
	document.getElementById("json").appendChild(document.createTextNode("action : " + cmd + "\n"));
	if (jsonData !== null) { 		
		document.getElementById("json").appendChild(document.createTextNode(JSON.stringify(jsonData, null, 2)));
	}
}

// ===============================================
function createDebugPanel(){			
	 var parentElem = document.body;
	 var newDiv = document.createElement('div');
	 newDiv.innerHTML = '<div id="json" class="cs_list_json"></div>';
	 parentElem.appendChild(newDiv);
}
*/

// ===============================================
function onClickMute(evt) {
  "use strict";
  evt.stopPropagation();
  setMute();
}

// ===============================================
function onClickFavorites(evt) {
  "use strict";
  evt.stopPropagation();
  // console.info(curIndex + " | uniqueVal " + uniqueVal(favorites, curIndex));
  if (uniqueVal(favorites, curIndex)) {
    favorites.push(curIndex);
    if (favorites.length > 4) {
      favorites.splice(0, 1);
    }
    createFavoritesList();
    saveAppState();
  }
}

// ===============================================
function createFavoritesList() {
  "use strict";
  var buttons = "";
  if (isDefined(favorites)) {
    for (var i = 0; i < favorites.length; i++) {
      buttons +=
        '<button id="fav' +
        Number(favorites[i]) +
        '" class="fav_st" onclick="playFavorites(event)">' +
        radioList[Number(favorites[i])].title +
        "</button>";
    }
    $("#fav_box").html(
      '<button class="fav_st label" >favorites •</button>' + buttons
    );
  }
}

// ===============================================
function playFavorites(evt) {
  "use strict";
  evt.stopPropagation();
  // console.info("playFavorites " + evt.target.id.substr(3));
  curIndex = parseInt(evt.target.id.substr(3), 10);
  // console.info("playFavorites " + curIndex);
  nextStation();
  window.setTimeout(function () {
    defMode = true;
    swapDigitalAnalog();
    playRadio(curIndex);
  }, 2000);
}

// ===============================================
function nextFavorites(idx) {
  "use strict";
  var id = favorites[idx];
  curIndex = id;
  defMode = false;
  nextStation();
  window.setTimeout(function () {
    defMode = true;
    swapDigitalAnalog();
    playRadio(curIndex);
  }, 2000);

  // defMode = false;
  // nextStation();
  // window.setTimeout(function () {
  //   defMode = true;
  //   // swapDigitalAnalog();
  //   playRadio(curIndex);
  // }, 2000);

  // if (isDefined(id)) {
  //   curIndex = id;    
  //   clearInterval(cycleID);
  //   cycleID = setInterval(setFreq, 10);
  //   playRadio(curIndex);
  // }
  // $("#json-cmd").html("id : " + id + " | IDX " + idx);
}

//===============================================
function uniqueVal(arr, cur_val) {
  return arr.join().indexOf("" + cur_val) >= 0 ? false : true;
}

// ===============================================
function formatSS(s, size, sym) {
  // var s = num+"";
  while (s.length < size) s = sym + s;
  return s;
}

// ===============================================
// Html5Storage
// ===============================================
function supportsHtml5Storage() {
  try {
    return "localStorage" in window && window["localStorage"] !== null;
  } catch (e) {
    return false;
  }
}

// ===============================================
function resumeAppState() {
  "use strict";
  console.warn("resumeAppState");
  if (!supportsHtml5Storage()) {
    console.warn("Not supports Local Storage");
    // return false;
  } else {
    if (localStorage["favorites"] !== null) {
      if (isDefined(localStorage["favorites"])) {
        favorites = localStorage["favorites"].split(",");
        if (localStorage["cur_index"] !== null) {
          curIndex = localStorage["cur_index"];
        } else {
          curIndex = 0;
        }
        console.info(
          "favorites : " + favorites[0] + " | cur_index : " + curIndex
        );
        // createFavoritesList();
      }
      // return true;
    }
  }
}

// ===============================================
function saveAppState() {
  "use strict";
  if (supportsHtml5Storage()) {
    localStorage["favorites"] = favorites.join();
    localStorage["cur_index"] = curIndex;
    console.info(
      "saveAppState | favorites : ",
      localStorage["favorites"] + " | cur_index : " + curIndex
    );
    // return true;
  } else {
    var sendData = {
      favorites: favorites.join() || null,
      cur_index: curIndex,
    };
    console.info("saveAppState | sendData : " + JSON.stringify(sendData));
    sendRequestToNativeApp(EVT_SAVE_PREFS, sendData);
    // return false;
  }
}

// ===============================================
function onClickTest(evt) {
  "use strict";
  evt.stopPropagation();
  favorites.length = 0;
  // sendRequestToNativeApp(EVT_WOKE_UP, { response: null });
  location.reload(true);
  // saveAppState();
  // window.setTimeout(function () {
  //   $(".overlay").fadeIn("slow", function () {
  //     location.reload(true);
  //   });
  // }, 2500);
}
