// ===============================================
// Main  |  main.js
// ===============================================

// =============================
// const
// =============================

// EVT_UPDATE = 1001;
// EVT_SETTINGS = 1002;

// CMD_BACK = 2000;
// CMD_INIT = 3000;
// CMD_UPDATE = 4000;
// ===============================================
// variables
// ===============================================
var start_day;
var start_night;
var auto_start_night_mode;
var swap_frequency;
var nightMode = false;
var last_state = false;

var withAPP = false;
// var obj_hour;
// var obj_min;
// var obj_sec;
// var obj_cur_month_gear;
// var obj_cur_day_week;

var flag = false;
// ===============================================

// Start =============================
window.onload = function() {
  "use strict";
  //console.info("HTML onload");
  //sendRequestToAplication(EVT_READY, {"browser": getBrowser()});
  //document.getElementById(body).add('<div class="cs_list_json">+++</div>');
  if (!isTouchDevice()) {
    // stats = new Stats();
    // stats.showPanel(1);
    // document.body.appendChild(stats.dom);
    init({});
    updateWidget(null);
  }

  ResizeWidget();
  withAPP = isNativeApplication();

  // createDebugPanel();
  // outputJson(syntaxHighlight({ evt: "Start" }));
  addOnmessageEventsListener();
  // addCrossdomainEventsListener();
  window.setTimeout(function() {
    sendRequestToNativeApp(EVT_READY, {
      browser: getBrowser(),
      ui: "project_watch"
    });
    $(".overlay").fadeOut("slow", function() {});
    this.setInterval(setDigitalTime, 1000);
  }, 2500);
  setCurrentTime();
};

// ===============================================
window.onresize = function() {
  "use strict";
  ResizeWidget();
};

// ===============================================
// function init() {
//   obj_hour = document.getElementById("hour");
//   obj_min = document.getElementById("min");
//   obj_sec = document.getElementById("sec");
//   obj_cur_month_gear = document.getElementById("cur_month_gear");
//   // obj_cur_day_week = document.getElementById("day_week");
//   updateWidget(null);
// }

// ===============================================
function ResizeWidget() {
  "use strict";
  var scale = Math.min(window.innerWidth, window.innerHeight) / 800;
  // var obj_win = document.getElementById("win");

  // if (obj_win != null) {
  //   scale = Math.min(obj_win.clientWidth, obj_win.clientWidth) / 1280;
  // }
  scale = Math.min($("#win").innerWidth(), $("#win").innerHeight()) / 720;
  var a_scale = nightMode ? 0.65 : 1;
  var d_scale = nightMode ? 1 : 0.75;
  var a_pos = nightMode ? -108 : -95;
  var d_pos = nightMode ? -22 : -10;
  $("#analog").css(
    "transform",
    "translate(" + Math.floor(a_pos) + "%, -50%) scale(" + scale * a_scale + ")"
  );
  $("#time").css(
    "transform",
    "translate(" + Math.floor(d_pos) + "%, -50%) scale(" + scale * d_scale + ")"
  );
  $("#time").css("color", nightMode ? "#84ffff" : "#FFFFFF");
  $(".bg").css("background-position", nightMode ? "center 72%" : "center 45%");
  last_state = nightMode;
}

// ===============================================
function restartAnimation() {
  "use strict";
  //console.info("restartAnimation");
  var el_hour = document.getElementById("arrow_hour");
  var el_min = document.getElementById("arrow_min");
  var el_sec = document.getElementById("arrow_sec");
  el_hour.classList.remove("run-animation");
  el_min.classList.remove("run-animation");
  el_sec.classList.remove("run-animation");
  /*
    el_hour.offsetWidth = el_hour.offsetWidth;
    el_min.offsetWidth = el_min.offsetWidth;
    el_sec.offsetWidth = el_sec.offsetWidth;
    */
  el_hour.classList.add("run-animation");
  el_min.classList.add("run-animation");
  el_sec.classList.add("run-animation");
  // $("#json-cmd").html("restartAnimation");
}
// ===============================================
function setDigitalTime() {
  "use strict";
  var MONTH = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  var WEEKDAY = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  var date = new Date();
  var hour = date.getHours();
  var min = date.getMinutes();
  var sec = date.getSeconds();
  $("#time").html(
    formatXX(hour, 2) +
      "<span>:</span>" +
      formatXX(min, 2) +
      "<span>:</span>" +
      formatXX(sec, 2) +
      (nightMode ? '<div class="time_desc">' : '<div class="time_desc dig">') +
      date.getDate() +
      ", " +
      MONTH[date.getMonth()] +
      " " +
      date.getFullYear() +
      " • " +
      WEEKDAY[date.getDay()] +
      "</div>"
  );

  if (sec === 0) {
    // night mode -----------------
    nightMode = hour < start_day || hour >= start_night;
    // if (auto_start_night_mode) {
    //   if (hour === start_day && min === 30) {
    //     sendRequestToNativeApp(EVT_NIGHT_MODE, { state: nightMode });
    //   }
    // } else {
    //   // ----------------------------
    //   if (min % swap_frequency === 0) {
    //     console.info(min);
    //     sendRequestToNativeApp(EVT_NEXT, { response: null });
    //   }
    // }
  }
  if (hour === start_day && min === 30) {
    sendRequestToNativeApp(EVT_WOKE_UP, { response: null });
  }
  // console.info("dig • " + nightMode + "| last_state " + last_state);
  if (last_state !== nightMode && sec === 0) {
    sendRequestToNativeApp(EVT_SYNC, { response: null });
    swapDigitalAnalog();
  }
}

// ===============================================
function setCurrentTime() {
  "use strict";
  var date = new Date();
  var CM = date.getMonth();
  //  var DW = date.getDay();
  //  var DD = date.getDate();
  var HH = date.getHours();
  var MM = date.getMinutes();
  var SS = date.getSeconds();
  var hour = HH > 12 ? HH - 12 : HH;
  restartAnimation();

  $("#hour").css(
    "-webkit-transform",
    "rotate(" + (hour * 30 + MM * 0.48 + SS * 0.06) + "deg)"
  );
  $("#min").css("-webkit-transform", "rotate(" + (MM * 6 + SS * 0.06) + "deg)");
  $("#sec").css("-webkit-transform", "rotate(" + SS * 6 + "deg)");
  $("#cur_month_gear").css("-webkit-transform", "rotate(" + CM * -30 + "deg)");

  // if (getBrowser() === "Firefox") {
  //   // obj_hour.style.transform =
  //   //   "rotate(" + (hour * 30 + MM * 0.48 + SS * 0.06) + "deg)";
  //   obj_min.style.transform = "rotate(" + (MM * 6 + SS * 0.06) + "deg)";
  //   obj_sec.style.transform = "rotate(" + SS * 6 + "deg)";
  //   obj_cur_month_gear.style.transform = "rotate(" + CM * -30 + "deg)";
  //   // obj_cur_day_week.style.transform = "rotate(" + DW * 51.428 + "deg)";
  // } else {
  //   // obj_hour.style.WebkitTransform =
  //   //   "rotate(" + (hour * 30 + MM * 0.48 + SS * 0.06) + "deg)";
  //   obj_min.style.WebkitTransform = "rotate(" + (MM * 6 + SS * 0.06) + "deg)";
  //   obj_sec.style.WebkitTransform = "rotate(" + SS * 6 + "deg)";
  //   obj_cur_month_gear.style.WebkitTransform = "rotate(" + CM * -30 + "deg)";
  //   // obj_cur_day_week.style.WebkitTransform = "rotate(" + DW * 51.428 + "deg)";
  // }
}

// ===============================================
function updateWidget(jsonData) {
  "use strict";
  //console.info("updateWidget");
  // restartAnimation();
  setCurrentTime();
  // setLed(jsonData);
}

// ===============================================
//function setLed(jsonData) {
//	"use strict";
//  if (jsonData === null) {
//    return;
//  }
//
//  var item = jsonData.resp_data;
//  if (item.bluetooth) {
//    document.getElementById("bluetooth").classList.remove("hide");
//  } else {
//    document.getElementById("bluetooth").classList.add("hide");
//  }
//  if (item.wifi) {
//    document.getElementById("wifi").classList.remove("hide");
//  } else {
//    document.getElementById("wifi").classList.add("hide");
//  }
//  if (item.gps) {
//    document.getElementById("gps").classList.remove("hide");
//  } else {
//    document.getElementById("gps").classList.add("hide");
//  }
//  if (item.airplane_mode) {
//    document.getElementById("air_mode").classList.remove("hide");
//  } else {
//    document.getElementById("air_mode").classList.add("hide");
//  }
//  if (item.battery > 2) {
//    document.getElementById("level_battery").classList.remove("low");
//  } else {
//    document.getElementById("level_battery").classList.add("low");
//  }
//  document.getElementById("level_battery").style.width =
//    item.battery * 10 + "%";
//}

// ===============================================
function onClickSettings(evt) {
  "use strict";
  evt.stopPropagation();
  //location.reload(true);
  sendRequestToAplication(EVT_SETTINGS, "settings", {});
}
// ===============================================
function onClickSwap(evt) {
  "use strict";
  evt.stopPropagation();
  nightMode = !nightMode;
  swapDigitalAnalog();
}

// ===============================================
function swapDigitalAnalog() {
  "use strict";
  console.info("swapDigitalAnalog | nightMode " + nightMode);
  ResizeWidget();
}

// ===============================================
function init(jsonData) {
  "use strict";
  start_day = jsonData.start_day || 6;
  start_night = jsonData.start_night || 19;
  swap_frequency = jsonData.swap_frequency || 1;
  auto_start_night_mode = jsonData.auto_start_night_mode || false;
}

// ===============================================
function JavaScriptCallback(cmd, jsonData) {
  "use strict";
  // console.info(
  //   "[] JavaScriptCallback cmd = " +
  //     cmd +
  //     " | data = " +
  //     JSON.stringify(jsonData)
  // );

  var response = jsonData[RESPONSE_JSON] || {};
  // outputJson(syntaxHighlight(response));
  // $("#json-cmd").html(
  //   "cmd : " +
  //     cmd +
  //     " | " +
  //     JSON.stringify(response)
  //       .split(",")
  //       .join(", ")
  // );

  switch (cmd) {
    case CMD_INIT:
      init(response);
      // sync ------------------------
      window.setTimeout(function() {
        sendRequestToNativeApp(EVT_SYNC, { response: null });
      }, 5000);
      window.setTimeout(function() {
        updateWidget(response);
      }, 1000);
      break;
    case CMD_BACK:
      backEvent();
      break;
    case CMD_WEATHER_DATA:
      // forecastWeather(response);
      break;

    // --------------------------
    // Watch
    // --------------------------
    case CMD_SWAP:
      nightMode = !nightMode;
      // $("#json-cmd").html("cmd : " + cmd + " | nightMode • " + nightMode);
      ResizeWidget();
      break;
    // --------------------------

    case CMD_UPDATE:
      updateWidget(response);
      break;
    case CMD_REDIRECT:
      // backEvent();
      break;
    case EVT_EXO:
      console.info("[ trace ] JavaScriptCallback command " + cmd + " EVT_EXO");
      //  outputJson(syntaxHighlight(response));
      $("#json-cmd").html(
        "cmd : " +
          cmd +
          " | " +
          JSON.stringify(response)
            .split(",")
            .join(", ")
      );
      break;
    default:
      console.info("[ ] JavaScriptCallback command " + cmd + " not present");
      break;
  }
}

// ===============================================

// ===============================================
function backPrevScreen() {
  "use strict";
  console.info("backPrevScreen");
}

function updateWeatherOutDoor(json) {}
/*
// ===============================================
function printJsonData(target, cmd, jsonData){
	if (withAPP) {return;}	
	document.getElementById("json").innerHTML = '<p style="color:#FC0">' + " json  : " + target + '</p>';
	document.getElementById("json").appendChild(document.createTextNode("action : " + cmd + "\n"));
	if (jsonData !== null) { 		
		document.getElementById("json").appendChild(document.createTextNode(JSON.stringify(jsonData, null, 2)));
	}
}

// ===============================================
function createDebugPanel(){			
	 var parentElem = document.body;
	 var newDiv = document.createElement('div');
	 newDiv.innerHTML = '<div id="json" class="cs_list_json"></div>';
	 parentElem.appendChild(newDiv);
}
*/
// ===============================================
function onClickTest(evt) {
  "use strict";
  evt.stopPropagation();
  sendRequestToNativeApp(EVT_WOKE_UP, { response: null });
  //location.reload(true);
}
