//* global CMD_INIT, EVT_READY, createNavigator */

/**
 * Created by Alex Dovbii on 27 May 2017.
 * Project HTML5UI
 * Application Main
 */

var DEBUG = false;
var withAPP = false;
var language = "en";
var root_icon = "icon/";
var config_url = "panel_ctrl_cmd.json";
var MOUSE_WHEEL = true;
var myScroll;
// ===============================================
window.onload = function () {
  // $("#content_container").fadeOut(0);

  withAPP = isNativeApplication();
  if (withAPP) {
    addOnmessageEventsListener();
    window.setTimeout(function () {
      sendRequestToNativeApp(EVT_READY, {
        browser: getBrowser(),
        ui: "project_remote",
      });
    }, 500);
  } else {
    // init(loadJSON(config_url));
    createUI(loadJSON(config_url));
  }

  //$("#content_container").fadeIn("slow");
};

// ===============================================
function JavaScriptCallback(cmd, jsonData) {
  // console.info("[] JavaScriptCallback cmd = " + cmd + " | data = " + JSON.stringify(jsonData));
  var response = jsonData[RESPONSE_JSON];
  switch (cmd) {
    case CMD_INIT:
      init(response);
      break;
    case CMD_BACK:
      backEvent();
      break;
    case APP_DIRECTORY:
      console.info(
        "[DIR] JavaScriptCallback cmd = " +
          cmd +
          " | resp_data = " +
          JSON.stringify(response)
      );
      createUI(response);
      break;
    default:
      console.info("[] JavaScriptCallback command " + cmd + " not present");
      break;
  }
}

// ===============================================
function init(jsonData) {
  // language = isDefined(jsonData["language"]) ? jsonData["language"] : "en";
  // DEBUG = isDefined(jsonData["debug"]) ? jsonData["debug"] : false;
  // //config_url = isDefined(jsonData["config_url"]) ? jsonData["config_url"] : "";
  // edit_mode = isDefined(jsonData["edit_mode"]) ? jsonData["edit_mode"] : false;
  // if (edit_mode) {
  // 	JavaScriptCallback(CMD_DIRECTORY, {"response": loadJSON(config_url)});
  // }
  // console.info("init : ", jsonData);
  // createUI(jsonData);
  console.info("init : " + JSON.stringify(jsonData));
  // createUI(loadJSON(config_url), false);
}
// ===============================================
function selectProgram(evt) {
  evt.stopPropagation();
  var cmd = $(evt.target).attr("data-cmd");

  // scroll(0, -100);
  // $("content_container").animate({
  //   scrollTop: "+=100px",
  // });
  console.info("Click id • " + evt.target.id + " | " + evt.target);
  // $(evt.target).scrollIntoView(true);

  if (isUndefined(cmd) || $(evt.target).hasClass("blocking")) {
    return;
  }

  $(".item").toggleClass("select", false);
  $(".prog_grp").toggleClass("select", false);
  // if ($(evt.target).hasClass("ext")) {
  //   $(".ext").toggleClass("blocking", true);
  // } else {
  //   $(".ext").toggleClass("blocking", false);
  // }

  // myScroll.refresh();
  // window.setTimeout(function () {
  //   if (isDefined(myScroll) ) {
  //     myScroll.scrollToElement(("#" + $(evt.target).id), 1500, null, null, IScroll.utils.ease.circular);
  //   }
  // }, 10);
  // myScroll.scrollTo(0, myScroll.maxScrollY, 200, IScroll.utils.ease.circular);

  $(evt.target).toggleClass("select", true);

  console.log(" selectProgram | json:", cmd);
  //console.log(" selectProgram | :", $(evt.target).html());
  var sendData = { cmd: cmd };
  if (isUndefined(cmd)) {
    return;
  }
  // myScroll.scrollTo(0, -240, 1200, IScroll.utils.ease.circle);
  window.setTimeout(function () {
    $(evt.target)[0].scrollIntoView(true);
    if (withAPP) {
      sendRequestToNativeApp(EVT_UI, sendData);
    } else {
      delphiRequest(sendData);
    }
  }, 150);
}
// function scrollToBottom(e) {
//   e.scrollTop = e.scrollHeight - e.getBoundingClientRect().height;
// }
// function scrollDown() {
//   document.getElementById("list_container").scrollTop = document.getElementById(
//     "list_container"
//   ).scrollHeight;
// }
// ===============================================
function itemClick(evt) {
  evt.stopPropagation();

  // $(".item").toggleClass("select", false);
  var cmd = $(evt.target).attr("data-cmd");
  var sendData;
  if ($(evt.target).hasClass("item_toggle")) {
    $(evt.target).toggleClass("select");
    var state = $(evt.target).hasClass("select");
    sendData = { cmd: cmd, state: state };
    //scrollToBottom($(evt.target));
  } else {
    // $(".item").toggleClass("select", false);
    var sendData = { cmd: cmd };
  }
  // scrollDown()
  console.log("itemClick | json:", JSON.stringify(sendData));

  if (isUndefined(cmd)) {
    return;
  }

  // var scroll=document.getElementById("content");
  // scroll.scrollTop = -1000 +"px"; //scroll.scrollHeight;

  // $("content_container").animate({
  //   scrollTop: "+=100px",
  // });
  //$("#list_container").scrollBy(0, 100);
  // scroll(0, -100);
  // $(evt.target).scrollIntoView();
  // window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
  // var element = document.querySelector(".select");
  // $(evt.target).scrollIntoView();
  // $("#content").animate({scrollTop: 9999});

  window.setTimeout(function () {
    $(evt.target)[0].scrollIntoView(true);
    if (withAPP) {
      sendRequestToNativeApp(EVT_UI, sendData);
    } else {
      delphiRequest(sendData);
    }
  }, 150);
}

// ===============================================
// function clickReturn(evt) {
//   evt.stopPropagation();
//   var sendData = {cmd: CMD_BACK};
//   delphiRequest(sendData);
//   console.log("clickReturn:");
//   // sendRequestToNativeApp(EVT_UI_RETURN, { ui: "project_directory" });
// }

// function createVerticaleWrapper(id, content, target_evt) {
//   // | .wrapper | .scroller |
//   //removeObject(id);
//   var SCROLLER = '<div class="scroller">' + content + "</div>";
//   var WRAPPER = '<div id="' + id + '" class="wrapper">' + SCROLLER + "</div>";
//   var elID = "#" + id;
//   window.setTimeout(function () {
//     myScroll = new IScroll(elID, {
//       probeType: 3,
//       tap: true,
//       mouseWheel: MOUSE_WHEEL,
//     });

//     // myScroll.on("scroll", textScrollEvents);
//     // document.getElementById(id).addEventListener("tap", target_evt, false);
//   }, 500);
//   return WRAPPER;
// }

// ===============================================
function createUI(data) {
  var list = data["screens"] || [];

  // return;
  /*
	if (isUndefined(projects_list)) {
		//$("#content_container").fadeIn("slow");
		
		//return;
	}
	*/
  console.log("createUI | screens: " + JSON.stringify(list));
  if (list.length === 0) {
    // $("#content_container").fadeIn("slow");
    return;
  }

  // projects_list = [];
  var content =
    '<div class="list_container" onclick="selectProgram(event);">' +
    //  '<div onclick="selectProgram(event);">' +
    getProgramsList(list) +
    "</div>";
  // '<div class="cs_directory shadow">PROGRAMMS</div>' +
  // '<div class="cs_directory shadow"><span>PROGRAMMS</span></div>' +
  // '<button class="cs_button" onclick="clickReturn(event);"></button>';

  $("#content_container").html(
    // createVerticaleWrapper("wraper_glossary_list", content, itemClick)
    '<div id="content" class="cs_content">' + content + "</div>"
  );
  //$("#content_container").fadeIn("slow");
}

// ===============================================
function getProgramsList(list) {
  var content = "";
  for (var i = 0; i < list.length; i++) {
    var itemList = new programItem(list[i]);
    content += itemList.create();
  }
  return content;
}

// ===============================================
function getCommandList(list) {
  var content = "";
  for (var i = 0; i < list.length; i++) {
    var itemList = new itemListProto(list[i]);
    content += itemList.create();
  }
  return '<ul onclick="itemClick(event);">' + content + "</ul>";
}

// ===============================================
function programItem(item) {
  if (isUndefined(item)) {
    return "";
  }
  var ICON = isPresent(item["icon"])
    ? ' style="background-image:url(' + root_icon + item["icon"] + ');"'
    : "";
  var CLASS = ' class="' + "item" /*["class"]*/ + '"';
  //var CLASS = ' class="' + item["class"] + '"';
  var sendData = {};
  //   sendData["url"] = item["url"];
  //   sendData["options"] = item["options"];
  //   sendData["icon"] = item["icon"];
  var dataJson = " data-cmd=" + item["cmd"];

  this.create = function () {
    var type_app = item["external"] ? " ext" : "";
    var content =
      '<div id="c' +
      item["cmd"] +
      '" class="prog_grp' +
      type_app +
      '"' +
      dataJson +
      ">" +
      //'<li' + CLASS + sendData + ' data-url="' + item["url"] + '" data-icon="' + item["icon"] + '">' +
      '<div class="program"' +
      CLASS +
      ">" +
      '<div class="icon_container">' +
      '<div class="icon_img"' +
      ICON +
      '">' +
      "</div>" +
      "</div>" +
      '<div class="program_name">' +
      item.program +
      // "<span> | ver" +
      // BULL_SP +
      // item["version"] +
      // "</span>" +
      // '<span class="cs_desk">' +
      // item["desc"] +
      // "</span>" +
      "</div>" +
      "</div>" +
      getCommandList(item.items) +
      "</div>";
    return content;
  };
}

// ===============================================
function itemListProto(item) {
  if (isUndefined(item)) {
    return "";
  }
  var ICON = isPresent(item["icon"])
    ? ' style="background-image:url(' + root_icon + item["icon"] + ');"'
    : "";
  var CLASS = ' class="' + "item" /*["class"]*/ + '"';
  //var CLASS = ' class="' + item["class"] + '"';
  //var sendData = {};
  //   sendData["url"] = item["url"];
  //   sendData["options"] = item["options"];
  //   sendData["icon"] = item["icon"];
  //var dataJson = " data-json=" + JSON.stringify(sendData);
  var dataJson = " data-cmd=" + item["cmd"];
  var css_toggle = item["toggle"] ? "item_toggle" : "item";
  this.create = function () {
    var content =
      //'<li' + CLASS + sendData + ' data-url="' + item["url"] + '" data-icon="' + item["icon"] + '">' +
      '<li class="' +
      css_toggle +
      '"' +
      dataJson +
      ">" +
      '<div class="icon_container">' +
      '<div class="icon_img"' +
      ICON +
      "></div>" +
      "</div>" +
      '<div class="item_name">' +
      item["name"] +
      //   "<span> | ver" +
      //   BULL_SP +
      //   item["version"] +
      //   "</span>" +
      '<span class="cs_desk">' +
      item["desc"] +
      "</span>" +
      "</div>" +
      "</li>";
    return content;
  };
}

// ===============================================
function delphiRequest(data) {
  "use strict";
  if (isDefined(data)) {
    console.info("delphiRequest : " + JSON.stringify(data.cmd));
    // var state = isDefined(data.state) ? ("," + (data.state ? 1 : 0)) : ""
    var state = isDefined(data.state) ? ("," + data.state) : ""
    window.location = "#CMD:" + data.cmd +  state;
  }
}
