/**
 * Created by Alex Dovbii on 02 Jun 2017.
 * Project HTML5 UI|UX
 * Interface
 * Android Studio Application 
 *
 * JavascriptInterface Android Constants -----------------------
 * public static final String INTERFACE_NAME = "NativeApplication";
 * function |   callNative   | 
 * public final void callNative( int request, final String jsonString )
 * -------------------------------------------------------------
 * JavascriptInterface HTML Constants --------------------------
 * init events for all projects
 * var EVT_MAIN_TEST = 0;	  
 * var EVT_READY = 10;
 * function | callJavaScript |
 */
var INTERFACE_NAME = "NativeApplication";
var REQUEST_JSON = "request",
	RESPONSE_JSON = "response";
// reserved for Html5UI --------
var APP_DIRECTORY = 20;
var EVT_UI = 5000;
var EVT_UI_RETURN = 22;
// -----------------------------

// init events and command -----
// for all projects ------------
var EVT_MAIN_TEST = 0;	  
var EVT_READY = 1;
var CMD_INIT = 1000;

// debug commands & events -----
var CMD_BACK = 3;
var EVT_TARGET_GEOPOSITION = 5;
var CMD_TARGET_COMPLETE = 6;
var CMD_UPDATE_GEOPOSITION = 7;
var CMD_AUTO_UPDATE_GEOPOSITION = 8;
var CMD_TARGET_GEOPOSITION = 9;
var EVT_NAVIGATION = 10;
var CMD_REDIRECT = 32;
var EVT_EXIT = 35;

var CMD_DEBUG = 13;
var EVT_EXO = 777;
var EVT_EXO_RESPONSE = 888;

// ===============================================
function sendRequestToNativeApp(cmd, jsonData) {
    //console.info("sendRequestToAplication | ", requestID, cmd, jsonData);

    if (withAPP) {
        window.setTimeout(function () {
            try {
                if (typeof jsonData === "string")
					window[INTERFACE_NAME].callNative(cmd, jsonData);
                else
					window[INTERFACE_NAME].callNative(cmd, JSON.stringify(jsonData));
            }
            catch (err) {
            }
        }, 10);
    }  else {
		// console.info("sendRequest cmd= " + cmd + " data=" + JSON.stringify(jsonData));
		sendMesssegeWindowParent(REQUEST, cmd, jsonData);
	}
}

// ===============================================
function isNativeApplication() {
    try {
		window[INTERFACE_NAME].callNative(EVT_MAIN_TEST, "{}");
		console.info("[+] isNativeApplication | Application Present");
        return true;
    }
    catch (err) {
        console.info("[-] isNativeApplication | Application not Present");
        return false;
    }
}