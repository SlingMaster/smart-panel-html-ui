// ===============================================
function createDebugUI() {
  "use strict";
  var htmlCode =
    // grid components -->
    '<div id="grid" class="grid">' +
    '<div class="grid-canvas"></div>' +
    '<div class="grid-info"></div>' +
    "</div>" +
    // -------------------
    '<div class="btns_container" onclick="debugForecast(event);">' +
    '<button class="btn" data-id="change_season">12 Month</button>' +
    '<button class="btn" data-id="season">Season</button>' +
    '<button class="btn" data-id="tod_visual">Tod</button>' +
    '<button class="btn" data-id="cloud">Cloud</button>' +
    '<button class="btn" data-id="clear">Clear</button>' +
    '<button class="btn" data-id="rain">Rain</button>' +
    '<button class="btn" data-id="snow">Snow</button>' +
    '<button class="btn" data-id="storm">Storm</button>' +
    '<button class="btn" data-id="grid">Grid</button>' +
    '<button class="btn" data-id="update">Update</button>' +
    '<button class="btn" data-id="reset">Reset</button>' +
    "</div>";
  return htmlCode;
}

// ===============================================
function debugForecast(evt) {
  "use strict";
  evt.stopPropagation();

  // var key = $(evt.target).text();
  var key = $(evt.target).attr("data-id");
  // console.info(key);
  window.setTimeout(function() {
    switch (key) {
      case "change_season":
        fast_time = !fast_time;
        if (fast_time) {
          curMonth = 0;
          seasonTimer = setInterval(seasonScript, 6000);
        } else {
          clearInterval(seasonTimer);
          DateTime();
        }
        return;
      case "season":
        season++;
        if (season >= 4) {
          season = 0;
        }
        break;
      case "tod_visual":
        tod_visual++;
        if (tod_visual > 3) {
          tod_visual = 0;
        }
        break;
      case "cloud":
        cloudiness++;
        if (cloudiness > 3) {
          cloudiness = -1;
        }
        break;
      case "clear":
        precipitation = precipitation !== 0 ? 0 : 3;
        $("#fps-output").html("•");
        break;
      case "rain":
        precipitation = precipitation !== 4 ? 4 : 5;
        //rain = !rain;
        break;
      case "snow":
        precipitation = precipitation !== 6 ? 6 : 7;
        break;
      case "storm":
        precipitation = precipitation !== 8 ? 8 : 9;
        thunderstorm = !thunderstorm;
        break;
      case "grid":
        $(".grid").toggle();
        $(".cs_json_list").slideToggle("hide");
        break;
      case "update":
        sendRequestToNativeApp(EVT_WEATHER, { response: null });
        break;
      case "reset":
        window.location.reload(true);
        return;
    }
    snow = precipitation == 3 || precipitation == 6 || precipitation == 7;
    rain = precipitation == 3 || precipitation == 4 || precipitation == 5;
    $("#canvas").toggleClass("show", snow || rain);
    updateUI();
    $(".time").html(
      TODS[tod_visual] +
        " | Cloud: " +
        cloudiness +
        " | Precipitation: " +
        precipitation +
        " | Night: " +
        (tod_visual == 0)
      // +" | Season: " + season
    );
  }, 250);
}
