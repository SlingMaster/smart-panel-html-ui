/**
 * Created by Alex Dovbii on 18 May 2017.
 * Project AutoPilot
 * Constants 
 */
// ===============================================
// constants
// ===============================================
var BULL = "&#8226;";
var BULL_SP = " &#8226; ";
var SPLASH_SHOW_DELAY = 2000;



var CONTENT_PATCH = "img/"; 	
var MAP_ICON_PATCH = "img/map_icon/"; 
var EVT_NIGHT_MODE = 201;
