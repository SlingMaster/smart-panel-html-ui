// constants --
// var MONTH_RU = [
//   "Январь",
//   "Февраль",
//   "Март",
//   "Апрель",
//   "Май",
//   "Июнь",
//   "Июль",
//   "Август",
//   "Сентябрь",
//   "Октябрь",
//   "Ноябрь",
//   "Декабрь"
// ];
var MONTH = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];
var WEEKDAY = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday"
];
var start_day;
var start_night;
var auto_start_night_mode;
var swap_frequency;
var nightMode = false;
var last_state = true;

var seasonTimer;
var fast_time = false;
var TODS = ["Night", " Morning", "Day", "Evening"];
var FPS = 24;
var WEB_URL;
// variables --
var DEBUG = false;
var withAPP = false;
var precipitation = 0;
var cloudiness = 0;
// var night = false;
var thunderstorm = false;
var snow = false;
var rain = false;
var season = 0;
var canvas, ctx;
//var stats;
// var clear = false;
var curMonth = 0;
var tod_visual = 0;
//  frame animation =================================
var animationFrame =
  window.requestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  function(callback) {
    // callback();
    //window.setTimeout(, 1000);
    window.setTimeout(callback, 1000 / FPS);
  };
// ===============================================
window.onload = function() {
  "use strict";
  if (!isTouchDevice()) {
    // stats = new Stats();
    // stats.showPanel(1);
    // document.body.appendChild(stats.dom);
    init({});
  }

  withAPP = isNativeApplication();
  // outputJson(syntaxHighlight({ evt: "Start" }));
  createUI();
  // canvas ======================================
  canvas = document.getElementById("canvas");
  ctx = canvas.getContext("2d");

  addOnmessageEventsListener();
  addCrossdomainEventsListener();

  window.setTimeout(function() {
    sendRequestToNativeApp(EVT_READY, {
      browser: getBrowser(),
      ui: "project_weather"
    });
    $(".overlay").fadeOut("slow", function() {
      console.info("load main");
      // set current time and update inveroment  =====
      setInterval(DateTime, 60000);
      window.requestAnimationFrame = animationFrame;
      console.info("next initSnow()");
      initSnow();
      console.info("next loop()");
      loop();
    });
  }, 2500);

  // window.setTimeout(function() {
  //   $(".grid").fadeOut(0);
  //   $(".cs_json_list").slideToggle(0, false);
  // }, 500);
};
// ===============================================
function createUI() {
  "use strict";
  var content =
    '<div class="panorama">' +
    // fps -------------------------------
    // '<div class="debug_title">' +
    // '<span id="fps-output">•</span>' +
    // '<span id="cmd"></span>' +
    // "</div>" +
    // sky -------------------------------
    '<div id="sky-box">' +
    '<div id="sky" class="sky">' +
    "</div>" +
    '<div id="flash" class="sky-storm lightning flashit"></div>' +
    '<div id="sky-cur" class="sky-cur"></div>' +
    '<div class="visual_corection"></div>' +
    '<div class="stars"><div class="moon"></div></div>' +
    "</div>" +
    // gosprom ---------------------------
    '<div class="gosprom"></div>' +
    // gosprom night ---------------------
    '<div id="gosprom_night" class="gosprom-night">' +
    // gosprom windows light--------------
    '<div class="win_light" style="left:319px; top:280px; animation-duration:110s; -webkit-animation-duration:110s;"/>' +
    '<div class="win_light" style="left:273px; top:300px; animation-duration:17s; -webkit-animation-duration:17s;"/>' +
    '<div class="win_light" style="left:356px; top:372px; animation-duration:117s; -webkit-animation-duration:117s;"/>' +
    '<div class="win_light" style="left:447px; top:373px; animation-duration:128s; -webkit-animation-duration:128s;"/>' +
    '<div class="win_light" style="left:366px; top:352px; animation-duration:123s; -webkit-animation-duration:123s;"></div>' +
    '<div class="win_light" style="left:376px; top:352px; animation-duration:123s; -webkit-animation-duration:123s;"></div>' +
    '<div class="win_light" style="left:366px; top:393px; animation-duration:111s; -webkit-animation-duration:111s;"></div>' +
    '<div class="win_light" style="left:598px; top:282px; animation-duration:130s; -webkit-animation-duration:130s;"/>' +
    '<div class="win_light" style="left:553px; top:305px; animation-duration:122s; -webkit-animation-duration:122s;"/>' +
    '<div class="win_light" style="left:631px; top:365px; animation-duration:145s; -webkit-animation-duration:145s;"/>' +
    "</div>" +
    // ----------------- end gosprom night

    // warning-light ---------------------
    '<div class="warning-light"></div>' +
    '<div class="warning-light clon"></div>' +
    // -----------------------------------

    '<div id="nature" class="nature"></div>' +
    '<div class="fog"></div>' +
    // canvas for rain and snow ----------
    '<canvas id="canvas" class="canvas" width="640" height="170"></canvas>' +
    "</div>" +
    // ====================== end panorama

    // text content --------------
    '<div class="content_box" onclick="clickToggleForecast(event);">' +
    "<div>" +
    '<span id="location"></span>' +
    '<span id="cur_date" class="cur_date" onclick="clickRunSeasonScript(event);"></span>' +
    "</div>" +
    "<div>" +
    '<span id="t" class="big_font"></span>' +
    '<span class="rose_wind">' +
    '<div class="wind_arrow"></div>' +
    "</span>" +
    '<span class="desc_outdor">' +
    '<div class="desc_humidity"></div>' +
    '<div class="desc_presure"></div>' +
    "</span>" +
    "</div>" +
    '<div class="forecast">•</div>' +
    "</div>" +
    '<div id="forecast" class="forecast_box" onclick="clickToggleForecast(event);">' +
    '<div class="spinner wf"></div>' +
    "</div>";
  // --------------------------

  $("#content").html(content);
  DateTime();

  // debug ============================
  // $("#content").append(createDebugUI());
}

// ===============================================
function setCurrentSeason(curMonth) {
  "use strict";
  console.info("curMonth : " + curMonth);

  if (curMonth === 11 || curMonth < 2) {
    season = 0; //"winter";
  } else {
    if (curMonth > 1 && curMonth < 5) {
      season = 1; //"spring";
    } else {
      if (curMonth > 4 && curMonth < 8) {
        season = 2; //"spring";
      } else {
        if (curMonth > 7 && curMonth < 12) {
          season = 3; // "autom";
        }
      }
    }
  }
}
// ===============================================
function DateTime() {
  "use strict";
  if (fast_time) {
    return;
  }
  var d = new Date();
  var hour = d.getHours();
  var minutes = d.getMinutes();
  curMonth = d.getMonth();
  tod_visual = getTodVisual();
  setCurrentSeason(curMonth);
  $("#location").html(
    '<div id="sync" class="spinner"></div>' +
      '<div class="time">' +
      formatXX(hour, 2) +
      ":" +
      formatXX(d.getMinutes(), 2) +
      " • KHARKIV" +
      "</div>"
  );

  $("#cur_date").html(
    d.getDate() +
      ", " +
      MONTH[curMonth] +
      " • " +
      WEEKDAY[d.getDay()] +
      " • " +
      d.getFullYear()
  );

  getWeatherOutDoor();
  // monning mode -----------------
  // console.warn(hour, (start_day + 1), minutes);
  if (hour === (start_day + 1) && minutes === 30) {
    sendRequestToNativeApp(EVT_SHOW_TIME, { response: null });
  }

  // night mode -----------------
  // nightMode = hour < start_day || hour >= start_night;
  // if (last_state !== nightMode) {
  //   last_state = nightMode;
  //   sendRequestToNativeApp(EVT_BACK_LIGHT, { sleep_mode: nightMode });
  // }
  // if (auto_start_night_mode) {
  //   if (hour >= start_night) {
  //     sendRequestToNativeApp(EVT_NIGHT_MODE, { state: nightMode });
  //   }
  // }

  // // ----------------------------
  // if (minutes % swap_frequency === 0 && d.getSecond() < 10) {
  //   console.info(minutes);
  //   sendRequestToNativeApp(EVT_NEXT, { response: null });
  // }

  // updateUI();
}

// ===============================================
function loop() {
  "use strict";
  if (rain || snow) {
    // if (!isTouchDevice()) {
    //   stats.begin();
    // }
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if (snow) {
      snowfall();
    }
    if (rain) {
      initRain();
    }
    // $("#fps-output").html("• " + countFPS() + " | ");
    // if (!isTouchDevice()) {
    //   stats.end();
    // }
  }

  // debug fps ----------------
  // if (rain || snow) {
  //   $(".debug_title").fadeIn();
  // } else {
  //   $(".debug_title").fadeOut();
  // }
  // --------------------------

  animationFrame(loop);
}

// ===============================================
function JavaScriptCallback(cmd, jsonData) {
  "use strict";
  console.info(
    "[] JavaScriptCallback cmd = " +
      cmd +
      " | data = " +
      JSON.stringify(jsonData)
  );
  var response = jsonData[RESPONSE_JSON] || {};
  // outputJson(syntaxHighlight(response));
  $("#cmd").html(cmd);
  switch (cmd) {
    case CMD_INIT:
      // window.setTimeout(function() {
      init(response);
      //}, 1000);
      break;
    case CMD_BACK:
      // backEvent();
      break;
    case CMD_WEATHER_DATA:
      forecastWeather(response);
      break;
    case CMD_SWAP:
      toggleForecast();
      break;
    case CMD_SEASON_MAGIC:
      runSeasonScript();
      break;
    default:
      console.info("[] JavaScriptCallback command " + cmd + " not present");
      break;
  }
}

// ===============================================
function forecastWeather(response) {
  "use strict";
  var forecast = response.MMWEATHER.REPORT.TOWN.FORECAST[1] || {};
  var forecastStr = "N/A";
  var direction = 0;
  var d = new Date();
  // window.setTimeout(function() {
  //   outputJson(syntaxHighlight(forecast));
  // }, 1000);
  if (isDefined(forecast)) {
    precipitation = forecast.PHENOMENA.precipitation || 9;
    snow = precipitation === 3 || precipitation === 6 || precipitation === 7;
    rain = precipitation === 3 || precipitation === 4 || precipitation === 5;
    thunderstorm = precipitation === 8 || forecast.PHENOMENA.spower === 1 || 0;
    direction = 45 * (forecast.WIND.direction || 0);
    cloudiness = forecast.PHENOMENA.cloudiness;
    var t_air = forecast.TEMPERATURE.max;
    forecastStr =
      //' <span class="forecast-data">' +
      // forecast.day + ", " + MONTH[forecast.month || 0] + " • " + forecast.year + ' • ' +
      // TODS[forecast.tod || 0 ] + '</span>' +
      "Forecast for " +
      TODS[forecast.tod || 0] +
      "&nbsp;&nbsp;•&nbsp;&nbsp;" +
      '<span class="ic pressure">&nbsp;</span>Presure • ' +
      forecast.PRESSURE.max +
      " mmHq " +
      '<span class="ic ' +
      (t_air >= 0 ? "temperature" : "temperature_min") +
      '">&nbsp;</span>Temperature • ' +
      forecast.TEMPERATURE.max +
      "°C " +
      '<span class="ic humidity">&nbsp;</span>Humidity • ' +
      forecast.RELWET.max +
      " % " +
      '<span class="ic wind">&nbsp;</span>Wind • ' +
      forecast.WIND.min +
      "-" +
      forecast.WIND.max +
      " m/s " +
      '<span class="ic heat">&nbsp;</span>Heat • ' +
      forecast.HEAT.min +
      "°C" +
      '<span class="ic sync">&nbsp;</span>Last Sync • ' +
      formatXX(d.getHours(), 2) +
      ":" +
      formatXX(d.getMinutes(), 2);
  }
  $(".forecast").html(forecastStr);
  $(".wind_arrow").css({ WebkitTransform: "rotate(" + direction + "deg)" });
  $("#canvas").toggleClass("show", snow || rain);
  updateUI();
  if ($("#forecast").hasClass("show")) {
    createForecast(response.MMWEATHER.REPORT.TOWN.FORECAST || []);
  }
}

// ===============================================
function setSeason() {
  "use strict";
  var seasons = ["winter", "spring", "summer", "autom"];
  console.info("setSeason : " + seasons[season]);
  $("#nature").css(
    "backgroundImage",
    "url(weather/img/" + seasons[season] + ".png)"
  );
}

// ===============================================
function init(jsonData) {
  "use strict";
  //  language = isDefined(jsonData["language"]) ? jsonData["language"] : "en";
  WEB_URL = isDefined(jsonData.node_url) ? jsonData.node_url : "http://192.168.1.199";
  CHIP = isDefined(jsonData.chip_weather) ? parseInt(jsonData.chip_weather) : 772471;
  DEBUG = false; // isDefined(jsonData["debug"]) ? jsonData["debug"] : false;
  start_day = parseInt(jsonData.start_day) || 6;
  start_night = parseInt(jsonData.start_night) || 19;
  swap_frequency = parseInt(jsonData.swap_frequenc) || 2;
  auto_start_night_mode = jsonData.auto_start_night_mode || false;

  sendRequestToNativeApp(EVT_WEATHER, { response: null });

  // sync ------------------------
  window.setTimeout(function() {
    sendRequestToNativeApp(EVT_SYNC, { response: null });
  }, 5000);

  // set default state -----------
  setInterval(function() {
    sendRequestToNativeApp(EVT_WEATHER, { response: null });
  }, 3600000);

  // start ---------------------------------------
  // $(".overlay").fadeOut("slow");
  // local read node MCU ------
  getWeatherOutDoor();
  DateTime();
  // tod_visual = 0; //getTimeOfDay();
  // $(".time").html(" | tod_visual: " + tod_visual + " |");
  // updateSky();
  updateUI();
}

// ======================================
function updateUI() {
  "use strict";
  // var TODS = ["НОЧЬ", "УТРО", "ДЕНЬ", "ВЕЧЕР"];

  setSeason();
  setEnvironment();
}

// ======================================
function clickRunSeasonScript(evt) {
  "use strict";
  evt.stopPropagation();
  runSeasonScript();
}

// ======================================
function runSeasonScript() {
  "use strict";
  fast_time = !fast_time;
  $("#cur_date").toggleClass("select", fast_time);
  if (fast_time) {
    curMonth = 0;
    seasonTimer = setInterval(seasonScript, 6000);
  } else {
    clearInterval(seasonTimer);
    DateTime();
  }
}

// ======================================
function seasonScript() {
  "use strict";
  tod_visual++;

  if (tod_visual > 3) {
    tod_visual = 0;
    cloudiness++;
    if (cloudiness == 2 && curMonth == 0) {
      precipitation = 6;
      snow = true;
    }
    if (cloudiness == 2 && curMonth == 1) {
      precipitation = 7;
      snow = true;
    }
    if (cloudiness == 2 && curMonth == 4) {
      thunderstorm = true;
      precipitation = 8;
    }
    if (cloudiness > 3) {
      cloudiness = -1;
      precipitation = 0;
      curMonth++;
      if (curMonth > 11) {
        curMonth = 0;
      }
      setCurrentSeason(curMonth);
    }
  }
  $("#cur_date").html(MONTH[curMonth] + " • " + TODS[tod_visual]);
  console.info(
    "seasonScript : " + MONTH[curMonth],
    TODS[tod_visual],
    cloudiness
  );
  updateUI();
}
// ======================================
function getPrecipitationIcon(json) {
  "use strict";
  var icon = "";
  var precipitation = json.PHENOMENA.precipitation || 10;

  //  var spower = precipitation === 8 || json.PHENOMENA.spower == 1 || false;

  switch (precipitation) {
    case 3:
      icon = "snow_with_rain";
      break;
    case 4:
      icon = "rain1";
      break;
    case 5:
      icon = "rain2";
      break;
    case 6:
      icon = "snow1";
      break;
    case 7:
      icon = "snow2";
      break;
  }

  return icon;
}

// ======================================
function getCloudIcon(json) {
  "use strict";
  var icon = "";
  var cloudiness = json.PHENOMENA.cloudiness;
  var spower = precipitation === 8 || json.PHENOMENA.spower === 1 || false;
  var rain = precipitation === 4 || precipitation === 5 || false;

  if (spower) {
    icon = rain ? "thunderstorm_rain" : "thunderstorm";
  } else {
    switch (cloudiness) {
      case -1:
        icon = "cloud_fog";
        break;
      case 0:
        icon = "none";
        break;
      case 1:
        icon = "cloud1";
        break;
      case 2:
        icon = "cloud2";
        break;
      case 3:
        icon = "cloud3";
        break;
    }
  }

  return icon;
}
// ======================================
function forecastItem(json) {
  "use strict";
  if (json == null) {
    return;
  }
  var t_air = Math.round((json.TEMPERATURE.min + json.TEMPERATURE.max) * 0.5);
  var t_style = t_air < 0 ? "minus" : "plus";
  var src = json.tod === 0 || json.tod === 3 ? "/night/" : "/day/";
  var star =
    json.PHENOMENA.cloudiness === 3
      ? "none"
      : "url(weather/img" + src + "star.png);";

  var content =
    '<div class="forecast_item" style="background-image: ' +
    star +
    '">' +
    '<div class="i_data"><div><span>' +
    json.day +
    ", " +
    MONTH[Number(json.month) - 1] +
    "•" +
    json.year +
    "</span></div>• " +
    TODS[json.tod] +
    " •</div>" +
    '<div class="i_cloud" style="background-image: url(weather/img' +
    src +
    getCloudIcon(json) +
    '.png);">' +
    '<div class="i_precipitation" style="background-image: url(weather/img/precipitation/' +
    getPrecipitationIcon(json) +
    '.png);">' +
    '<span class="' +
    t_style +
    '">' +
    (t_air > 0 ? "+" : "") +
    t_air +
    "°C</span></div></div>" +
    '<div class="desc_item">' +
    '<span class="ic pressure">Presure • ' +
    // json.PRESSURE.min +
    // "-" +
    json.PRESSURE.max +
    " mmHq" +
    "</span>" +
    // '<span class="ic ' +
    '<span class="ic humidity">Humidity • ' +
    json.RELWET.min +
    "-" +
    json.RELWET.max +
    " %" +
    '</span><span class="ic wind">Wind • ' +
    json.WIND.min +
    "-" +
    json.WIND.max +
    " m/s" +
    '</span><span class="ic heat">Heat • ' +
    json.HEAT.min +
    "°C</span>" +
    "</div>" +
    "</div>";
  return content;
}

// ======================================
function createForecast(json) {
  "use strict";
  var content = "";
  for (var i = 0; i < json.length; i++) {
    // debug weather icon -------
    // json[i].PHENOMENA.cloudiness = 3;
    // json[i].PHENOMENA.precipitation = 4;
    // json[i].PHENOMENA.spower = 1;
    // if (i == 1) {
    //   json[i].PHENOMENA.precipitation = 4;
    //   json[i].PHENOMENA.spower = 0;
    //   json[i].PHENOMENA.cloudiness = 1;
    // }
    // if (i == 2) {
    //   json[i].PHENOMENA.precipitation = 5;
    //   json[i].PHENOMENA.spower = 0;
    //   json[i].PHENOMENA.cloudiness = 2;
    // }
    // if (i == 3) {
    //   json[i].PHENOMENA.precipitation = 3;
    //   json[i].PHENOMENA.spower = 0;
    //   json[i].PHENOMENA.cloudiness = 3;
    // }
    // debug weather icon -------
    content = content + forecastItem(json[i]);
  }
  $("#forecast").html(
    '<div class="forecast_title">Daily weather forecast for Kharkiv • Ukraine</div>' +
      content
  );
}

// ======================================
function clickToggleForecast(evt) {
  "use strict";
  evt.stopPropagation();
  toggleForecast();
}

// ======================================
function toggleForecast() {
  "use strict";
  // sendRequestToNativeApp(EVT_SHOW_TIME, { response: null });
  // return;
  if ($("#forecast").hasClass("show")) {
    $("#forecast").toggleClass("show", false);
    $(".content_box").toggleClass("hide", false);
  } else {
    $(".content_box").toggleClass("hide", true);
    $("#forecast").toggleClass("show", true);
    sendRequestToNativeApp(EVT_WEATHER, { response: null });
  }
}
