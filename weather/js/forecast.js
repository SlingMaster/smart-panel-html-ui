var intervalID;
var FLAKE_MAX_SIZE = 1.5;
var T_CORRECTION = 3000;

// ======================================
function setEnvironment() {
  "use strict";
  var posX = 800;
  var posY = 220;
  var opacity = 0;
  var bild_opacity = 0;
  var dur = "4s";
  switch (cloudiness) {
    case -1:
      bild_opacity = 0.25;
      break;
    case 0:
      bild_opacity = 0;
      break;
    case 1:
      bild_opacity = 0.25;
      break;
    case 2:
      bild_opacity = 0.35;
      break;
    case 3:
      bild_opacity = 0.5;
      break;
  }
  if (tod_visual === 0) {
    bild_opacity = 1;
    posX = 920;
    posY = 20;
    dur = "5s";
    // opacity = cloudiness == 1 ? 0.7 : cloudiness == 0 ? 1 : 0;
    opacity = cloudiness === 0 ? 1 : 0;
  }
  if (tod_visual === 1) { 
    posY = 25;
    posX = 920;
    opacity = 0
    dur = "1s";
  }
  // if (tod_visual == 2) {
  //   posY = 220;
  //   posX = 300;  
  // }

  if (tod_visual === 3) {
    posX = 900;
    posY = 25;
    // opacity = cloudiness == 1 ? 0.7 : cloudiness == 0 ? 1 : 0;
    opacity = cloudiness === 0 ? 1 : 0;
  }

  if (tod_visual === 0) {
    //$("#gosprom_night").fadeIn(5000);
    $("#nature").css("opacity", 0.25);
  } else {
   // $("#gosprom_night").fadeOut(5000);
    $("#nature").css("opacity", 1);
  }
  $("#gosprom_night").css("opacity", bild_opacity);
  $(".moon").css({
    top: posY,
    left: posX,
    opacity: opacity,
    transitionDuration: dur
  });

  // $("#sky-default").css("opacity", thunderstorm ? 0 : night ? 0.4 : 1);
  // $("#sky-overcast").toggleClass("night", night);
  //updateTod();
  //  $(".stars").toggleClass("hide", cloudiness !== 0);
  updateSky();
}

// ======================================
function updateSky() {
  "use strict";
  var sky = "sky";
  var gamma = 1;
  switch (cloudiness) {
    case -1:
      sky =
        tod_visual === 3
          ? "sky-light-sunset-cloud"
          : tod_visual === 0
          ? season === 0 || season === 3
            ? "sky-overcast2"
            : "sky-overcast"
          : "sky-light-cloud";
      gamma = tod_visual === 0 ? 0.4 : 1;
      break;
    case 0:
      sky =
        tod_visual === 3 ? "sky-sunset" : tod_visual === 0 ? "sky-night" : "sky";
      break;
    case 1:
      sky = tod_visual === 3 ? "sky-light-sunset-cloud" : "sky-light-cloud";
      break;
    case 2:
      sky = "sky-cloudy";
      break;
    case 3:
      sky = season === 0 || season === 3 ? "sky-overcast2" : "sky-overcast";
      gamma = tod_visual == 0 ? 0.2 : 1;
      break;
  }
  if (thunderstorm) {
    sky = "sky-overcast";
  }

  $("#sky-cur").fadeIn(0);
  $(".moon").toggleClass("night", tod_visual === 0);
  $("#sky").css("opacity", gamma);
  $("#sky").css("backgroundImage", "url(weather/img/" + sky + ".jpg)");

  $("#flash").css("display", thunderstorm ? "block" : "none");
  $(".visual_corection").fadeOut("slow", function() {
    if (tod_visual === 0) {
      $(".visual_corection").toggleClass("sky_spring", false);
      $(".visual_corection").toggleClass("sky_summer", false);
      $(".visual_corection").toggleClass("sky_autom", false);
      $(".visual_corection").toggleClass(
        "sky_night",
        (tod_visual === 0 || cloudiness === -1) &&
          cloudiness !== 0 &&
          cloudiness < 3
      );
    } else {
      $(".visual_corection").toggleClass("sky_night", false);
      $(".visual_corection").toggleClass(
        "sky_spring",
        season === 1 && cloudiness < 3
      );
      $(".visual_corection").toggleClass(
        "sky_summer",
        season === 2 && cloudiness < 3
      );
      $(".visual_corection").toggleClass(
        "sky_autom",
        season === 3 && cloudiness < 3
      );
    }

    $("#sky-cur").fadeOut("slow", function() {
      $("#sky-cur").css("backgroundImage", "url(weather/img/" + sky + ".jpg)");
      $(".visual_corection").fadeIn(T_CORRECTION);
      $("#sky-cur").css("opacity", gamma);
      $;
    });
  });
  if (cloudiness === -1) {
    $(".fog").fadeIn("slow");
  } else {
    $(".fog").fadeOut("slow");
  }
}

// ======================================
// SNOW
// ======================================

var flakes = [],
  flakeCount = 75,
  mX = -100,
  mY = -100;

function snowfall() {
  "use strict";
  // ctx.clearRect(0, 0, canvas.width, canvas.height);
  var max = (precipitation === 6 || precipitation === 3)  ? 0 : 75;
  for (var i = 0; i < flakeCount + max; i++) {
    var flake = flakes[i],
      x = mX,
      y = mY,
      minDist = 150,
      x2 = flake.x,
      y2 = flake.y;

    var dist = Math.sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y));
    // dx = x2 - x,
    // dy = y2 - y;

    if (dist < minDist) {
      var force = minDist / (dist * dist),
        xcomp = (x - x2) / dist,
        ycomp = (y - y2) / dist,
        deltaV = force / 2;

      flake.velX -= deltaV * xcomp;
      flake.velY -= deltaV * ycomp;
    } else {
      flake.velX *= 0.98;
      if (flake.velY <= flake.speed) {
        flake.velY = flake.speed;
      }
      flake.velX += Math.cos((flake.step += 0.05)) * flake.stepSize;
    }

    ctx.fillStyle = "rgba(255,255,255," + flake.opacity + ")";
    flake.y += flake.velY;
    flake.x += flake.velX;

    if (flake.y >= canvas.height || flake.y <= 0) {
      reset(flake);
    }

    if (flake.x >= canvas.width || flake.x <= 0) {
      reset(flake);
    }

    ctx.beginPath();
    ctx.arc(flake.x, flake.y, flake.size, 0, Math.PI * 2);
    ctx.fill();
  }
  /// loop();
  // tick();
  // $("fps-output").html();
  // countFPS();
  // requestAnimationFrame(snowfall);
}

function reset(flake) {
	"use strict";
  flake.x = Math.floor(Math.random() * canvas.width);
  flake.y = 0;
  flake.size = Math.random() * FLAKE_MAX_SIZE + 1;
  flake.speed = Math.random() * 1 + 0.5;
  flake.velY = flake.speed;
  flake.velX = 0;
  flake.opacity = flake.size <= 2 ? 0.75 : 1;
  // flake.opacity = (Math.random() * 0.7) + 0.3;
}

function initSnow() {
  "use strict";
  var max = (precipitation === 6|| precipitation === 3)  ? 0 : 75;
  for (var i = 0; i < flakeCount + max; i++) {
    var x = Math.floor(Math.random() * canvas.width),
      y = Math.floor(Math.random() * canvas.height),
      // size = Math.random() * 2 + .5,
      size = Math.random() * FLAKE_MAX_SIZE + 0.5,
      // opacity = Math.random() * 0.7 + 0.3,
      speed = Math.random() * ((precipitation === 3 ? 10 : 1) + 0.5) ;

    flakes.push({
      //opacity: opacity,
      opacity: size <= 2 ? 0.75 : 1,
      speed: speed,
      velY: speed,
      velX: 0,
      x: x,
      y: y,
      size: size,
      stepSize: Math.random() / (precipitation === 3 ? 100 : 30),
      step: 0
    });
  }

  // snowfall();
}
// ======================================
// RAIN
// ======================================
function initRain() {
  "use strict";

  //  ctx.clearRect(0, 0, canvas.width, canvas.height);
  // if (!rain) {
  //   clearInterval(intervalID);
  //   return;
  // } else {
  //   intervalID = setInterval(draw, 30);
  // }
  // var ctx = canvas.getContext('2d');
  var w = canvas.width;
  var h = canvas.height;
  ctx.strokeStyle = "rgba(174,194,224,0.5)";
  ctx.lineWidth = 0.5;
  // ctx.lineCap = "round";

  var init = [];
  var maxParts = (precipitation === 4 || precipitation === 3) ? 200 : 300;
  var maxl = (precipitation === 4 || precipitation === 3) ? 1.25 : 2.5;
  var direct = (precipitation === 4 || precipitation === 3) ? 0 : -4;
  for (var a = 0; a < maxParts; a++) {
    init.push({
      x: Math.random() * w, 
      y: Math.random() * h,
      l: Math.random() * maxl,
      xs: direct + Math.random() * 2,
      ys: Math.random() * 5 + 10
    });
  }

  var particles = [];
  for (var b = 0; b < maxParts; b++) {
    ctx.lineWidth = 1; //Math.ceil(Math.random() * 1);
    particles[b] = init[b];
  }
  draw();
  function draw() {
    // ctx.clearRect(0, 0, w, h);

    for (var c = 0; c < particles.length; c++) {
      var p = particles[c];
      ctx.beginPath();
      ctx.moveTo(p.x, p.y);
      ctx.lineTo(p.x + p.l * p.xs, p.y + p.l * p.ys);
      ctx.stroke();
    }
    // tick();
    // $("fps-output").html();
    // countFPS();
    // requestAnimationFrame(move);
  }

  function move() {
    for (var b = 0; b < particles.length; b++) {
      var p = particles[b];
      p.x += p.xs;
      p.y += p.ys;
      if (p.x > w || p.y > h) {
        p.x = Math.random() * w;
        p.y = -20;
      }
    }
  }
}

// canvas.addEventListener("mousemove", function(e) {
//     mX = e.clientX,
//     mY = e.clientY
// });

// window.addEventListener("resize",function(){
//     canvas.width = window.innerWidth;
//     canvas.height = window.innerHeight;
// })

// init();

// ======================================
// cloud Weather Out Door
// ======================================
function getWeatherOutDoor() {
  "use strict";
  $("#sync").fadeIn(0);
  setTimeout(function() {
    sendCommandESP("web_srv", WEB_URL, "update", 0);
  }, 800);
}

// ======================================
function updateWeatherOutDoor(json) {
  "use strict";
  var t = json["air_t"];
  $("#t").html((t >= 0 ? "+" : "") + parseFloat(t).toFixed(1) + "°");
  $("#t").toggleClass("cool", t < 0);
  $(".desc_presure").html(parseFloat(json["air_p"]).toFixed(0) + " mm Hq");
  $(".desc_humidity").html(parseFloat(json["air_h"]).toFixed(1) + "%");
  $("#sync").fadeOut("slow");
}
